//
//  WADataStore.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/23/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import os.log


// DATA Model

struct CurrentConditionsData {
  var iconUrl : String?
  var conditionItems : [(key:String,value:String)] = []
  var temp: String?
  var weather: String?
  var primaryItems : [(key:String,value:String)] = []
  var secondaryItems : [(key:String,value:String)] = []
}

// A period of forecast, one day has two periods (day and night)
struct ForecastPeriod {
  var text: String?
  var title: String?
  var iconUrl : String?
  var icon : String?
}

struct ForecastPeriods {
  var periods:[ForecastPeriod] = []
}

// A whole single day
struct ForecastDay {
  var tempHigh: Double?
  var tempHighString: String {
    if let temp = tempHigh {
      return "\(Int(temp))"
    } else {
      return ""
    }
  }
  var tempLow: Double?
  var tempLowString: String {
    if let temp = tempLow {
      return "\(Int(temp))"
    } else {
      return ""
    }
  }
  
  var conditions: String?
  var possiblePrecipitation: Int?
  
  var quantityPrecipAmountDay: Double?
  var quantityPrecipAmountNight: Double?
  
  var weekday: String?
  var iconUrl : String?
  var icon : String?
  
  var textForecastDay: String?
  var textForecastNight: String?
}

struct ForecastDays {
  var periods:[ForecastDay] = []
}


struct HourData {
  var hour: Int?
  var ampm: String?
  var weekday: String?
  var temperature: Double?
  var temperatureString: String {
    if let temp = temperature {
      return "\(Int(temp))"
    }
    return ""
  }
  var iconUrl : String?
  var icon : String?
  
  var possiblePrecipitation: Int?
  var precipitationString: String?
  var skyValue: Int?
}

struct HourPeriods {
  var periods:[HourData] = []
  
}

extension HourData : CustomStringConvertible {
  var description : String {
    return "\(hour)\(ampm)\(temperatureString)"
  }
}



protocol DataStoreDelegate : class {
  func dataStore(_ controller: DataStore, didReceiveCurrentConditions conditionItems:[String], conditions:CurrentConditionsData)
  func dataStore(_ controller: DataStore, didReceiveHourly hourPeriods:HourPeriods)
  func dataStore(_ controller: DataStore, didReceiveHourlyTen hourPeriods:[HourPeriods])
  func dataStore(_ controller: DataStore, didReceiveForecast forecastDayData:ForecastDays, forecastPeriodsData:ForecastPeriods )
  func dataStore(_ controller: DataStore, primaryLocationTitle:String)
  func dataStore(_ controller: DataStore, updateForIconImage iconName:String)
  func dataStore(_ controller: DataStore, didReceiveSatteliteImage image:UIImage)
}

extension DataStoreDelegate {
  func dataStore(_ controller: DataStore, didReceiveCurrentConditions conditionItems:[String], conditions:CurrentConditionsData)
  {}
  func dataStore(_ controller: DataStore, didReceiveForecast forecastData:ForecastDays)
  {}
  func dataStore(_ controller: DataStore, didReceiveHourly hourPeriods:HourPeriods)
  {}
  func dataStore(_ controller: DataStore, didReceiveHourlyTen hourPeriods:[HourPeriods])
  {}
  func dataStore(_ controller: DataStore, didReceiveForecast forecastDayData:ForecastDays, forecastPeriodsData:ForecastPeriods )
  {}
  func dataStore(_ controller: DataStore, primaryLocationTitle:String)
  {}
  func dataStore(_ controller: DataStore, updateForIconImage iconName:String)
  {}
  func dataStore(_ controller: DataStore, didReceiveSatteliteImage image:UIImage)
  {}
}


/**
 Weather info dataStore is a layer between WAWeatherInfo service requests and application interface
 App objects asl the dataStore for data, and it is the dataStore that requests info from
 the weather service.
 
 Because JSON returned on the service call in WAWeatherInfo is lightly parsed for specific key fields

 This object performs additional parsing to obtain and create more specific data structures for return
 to the listener of responses via a delegate
 
 An image cache is used by this object.
 
 */

class DataStore: WeatherInfoDelegate {
  
  /// A delegate to update with results.
  weak var delegate: DataStoreDelegate?
  
  /// A weather info object.
  var weatherInfo = WeatherInfo()
  
  /// A placeholder for images that are unavailable.
  fileprivate lazy var imagePlaceholder = UIImage(named: "imageplaceholder")!
  
  /// A cache to store already retrieved images.
  fileprivate var imageCache: NSCache<AnyObject,AnyObject> = NSCache()
  
  /// A construct to storealready being retrieved images.
  fileprivate var pendingImage = [String:String]()
  
  /// A setting for the currently used icon set.
  fileprivate var iconSet: String? // use a,b,..g,i,k
  
  /// A lock for synchronized access to image cache.
  let lock = NSLock()
  
  
  @available(iOS 10.0, *)
  static let dataStore_log = OSLog(subsystem: "com.joker.weatherApp", category: "DataStore")
  
  /// Creates a Data Store and assigns itself as delegate to Info.
  init() {
    weatherInfo.delegate = self
    
    // Do not set iconSet to use values returned by the server
    //iconSet = "i"
  }
  
  // MARK: Public API
  
  /// Retrieves the current conditions without error handler.
  func getCurrentConditions() {
    getCurrentConditions(nil)
  }
  /// Retrieves the current conditions with error handler.
  func getCurrentConditions (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
    weatherInfo.getCurrentConditions(errorHandler)
  }
  /// Retrieves the forecast without error handler.
  func getForecast() {
    getForecast(nil)
  }
  /// Retrieves the forecast with error handler.
  func getForecast (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
    weatherInfo.getForecast(errorHandler)
  }
  /// Retrieves the ten day forecast without error handler.
  func getForecastTen() {
    weatherInfo.getForecastTen()
  }
  /// Retrieves the ten day forecast with error handler.
  func getForecastTen (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
    weatherInfo.getForecastTen(errorHandler)
  }
  /// Initiates retrieval of the sattellite image without error handler.
  func getSatellite() {
    getSatellite(nil)
  }
  /// Initiates retrieval of the sattellite image with error handler.
  func getSatellite (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
    weatherInfo.getSattelite(errorHandler)
  }
  /// Initiates retrieval of hourly data without error handler.
  func getHourly() {
    getHourly(nil)
  }
  /// Initiates retrieval of hourly data with error handler.
  func getHourly (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
    weatherInfo.getHourly(errorHandler)
  }
  /// Initiates retrieval of ten day hourly data without error handler.
  func getHourlyTen() {
    getHourlyTen(nil)
  }
  /// Initiates retrieval of ten day hourly data with error handler.
  func getHourlyTen (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
    weatherInfo.getHourlyTen(errorHandler)
  }
  
  
  // MARK: Helper
  
  /// Returns a `HourData` populated with data from `json`.
  func hourDataFromJson (_ json:JSON) -> HourData {
    
    var hourData = HourData()
    
    hourData.ampm = json["FCTTIME"]["ampm"].stringValue
    hourData.weekday = json["FCTTIME"]["weekday_name_abbrev"].stringValue
    if let intHour = Int(json["FCTTIME"]["hour"].stringValue) {
      if intHour > 12 {
        hourData.hour = intHour - 12
      } else {
        hourData.hour = intHour
      }
    }
    hourData.icon = json["icon"].stringValue
    hourData.iconUrl = json["icon_url"].stringValue
    hourData.temperature = Double(json["temp"]["english"].stringValue)
    hourData.possiblePrecipitation = Int(json["pop"].stringValue)
    hourData.skyValue = Int(json["sky"].stringValue)
    return hourData
  }
  
  /// Returns a `ForecastPeriod` populated with data from `json`.
  func forecastPeriodFromJson (_ json:JSON) -> ForecastPeriod {
    var forecastPeriod = ForecastPeriod()
    forecastPeriod.text = json["fcttext"].stringValue
    forecastPeriod.title = json["title"].stringValue
    forecastPeriod.icon = json["icon"].stringValue
    forecastPeriod.iconUrl = json["icon_url"].stringValue
    return forecastPeriod
  }
  
  /// Returns a `ForecastDay` populated with data from `json`.
  func forecastDayFromJson (_ json:JSON) -> ForecastDay {
    var dayForecast = ForecastDay()
    dayForecast.tempHigh = Double(json["high"]["fahrenheit"].stringValue)
    dayForecast.tempLow = Double(json["low"]["fahrenheit"].stringValue)
    dayForecast.weekday = json["date"]["weekday"].stringValue
    dayForecast.icon = json["icon"].stringValue
    dayForecast.iconUrl = json["icon_url"].stringValue
    dayForecast.conditions = json["conditions"].stringValue
    dayForecast.possiblePrecipitation = json["pop"].intValue
    dayForecast.quantityPrecipAmountDay = json["qpf_day"]["in"].doubleValue
    dayForecast.quantityPrecipAmountNight = json["qpf_night"]["in"].doubleValue
    return dayForecast
  }
  
  
  
  // MARK: - WAWeatherInfoDelegate
  
  //    func weatherInfo(_ controller: WAWeatherInfo, didReceiveHourlyTen hourTenPeriods:JSON)
  //    func weatherInfo(_ controller: WAWeatherInfo, didReceiveHourly hourPeriods:JSON)
  //    func weatherInfo(_ controller: WAWeatherInfo, didReceiveCurrentConditions conditions:JSON)
  //    func weatherInfo(_ controller: WAWeatherInfo, didReceiveForecast forecast:JSON)
  //    func weatherInfo(_ controller: WAWeatherInfo, didReceiveSatteliteImage image:UIImage)
  //    func weatherInfo(_ controller: WAWeatherInfo, didReceiveSattelite imageURLs:[String : AnyObject])
  
  
  /// Processes the response from a Ten Day Hourly data request
  func weatherInfo(_ controller: WeatherInfo, didReceiveHourlyTen json:JSON) {
    
    if #available(iOS 10.0, *) {
      os_log("Callback %@", log: DataStore.dataStore_log,  type:.info,"didReceiveHourlyTen")
    }
    
    var dayPeriods = HourPeriods()
    var tenDayPeriods:[HourPeriods] = []
    
    var currentYday = ""
    
    if let hourlyPeriods = json.array {
      
      if #available(iOS 10.0, *) {
        os_log("Process hourData", log: DataStore.dataStore_log, type:.debug)
      }
      
      for (_,hourlyPeriod) in hourlyPeriods.enumerated() {
        
        let hourData = hourDataFromJson(hourlyPeriod)
        
        if #available(iOS 10.0, *) {
          os_log("Process hourData", log: DataStore.dataStore_log)
        }
        
        let yday = hourlyPeriod["FCTTIME"]["yday"].stringValue
        
        if currentYday.isEmpty {
          currentYday = yday
        }
        
        if yday == currentYday {
          // Same day
          dayPeriods.periods += [hourData]
        } else {
          // New day
          tenDayPeriods += [dayPeriods]
          dayPeriods = HourPeriods()
          currentYday = yday
        }
      }
      
      tenDayPeriods += [dayPeriods]
    }
    
    
    DispatchQueue.main.async {
      self.delegate?.dataStore(self, didReceiveHourlyTen:tenDayPeriods)
    }
    
    
    for tenDayPeriod in tenDayPeriods {
      for period in tenDayPeriod.periods {
        if let icon = period.icon, let iconURLString = period.iconUrl {
          DispatchQueue.global().async {
            self.imageFor(icon, imageURLString: iconURLString)
          }
        }
      }
    }
    
  }
  
  /// Processes the response from a Hourly data request.
  func weatherInfo(_ controller: WeatherInfo, didReceiveHourly json:JSON) {
    
    if #available(iOS 10.0, *) {
      os_log("Callback %@", log: DataStore.dataStore_log, type:.info,"didReceiveHourly")
    }
    
    var hourPeriods = HourPeriods()
    
    if let hourlyPeriods = json.array {
      for (_,hourlyPeriod) in hourlyPeriods.enumerated() {
        let hourData = hourDataFromJson(hourlyPeriod)
        hourPeriods.periods.append(hourData)
      }
    }
    
    
    DispatchQueue.main.async {
      self.delegate?.dataStore(self, didReceiveHourly:hourPeriods)
    }
    
    for period in hourPeriods.periods {
      if let icon = period.icon,
        let iconURLString = period.iconUrl {
        
        DispatchQueue.global().async {
          self.imageFor(icon, imageURLString: iconURLString)
        }
      }
    }
  }
  
  /// Processes the response from a Forecast data request.
  func weatherInfo(_ controller: WeatherInfo, didReceiveForecast json:JSON) {
    
    if #available(iOS 10.0, *) {
      os_log("Callback %@", log: DataStore.dataStore_log, type:.info,"didReceiveForecast")
    }
    
    var forecastDays = ForecastDays()
    var forecastPeriods = ForecastPeriods()
    
    /// Text Forecast per period, day and night
    if let textForeCasts = json["txt_forecast"]["forecastday"].array {
      for (_,textForecast) in textForeCasts.enumerated() {
        let forecastPeriod = forecastPeriodFromJson(textForecast)
        forecastPeriods.periods.append(forecastPeriod)
      }
    }
    
    /// All day data Forecast per day
    
    if let foreCasts = json["simpleforecast"]["forecastday"].array {
      for (index,foreCast) in foreCasts.enumerated() {
        
        var dayForecast = forecastDayFromJson(foreCast)
        
        if let textForecast =  json["txt_forecast"]["forecastday"][index*2]["fcttext"].string {
          dayForecast.textForecastDay = textForecast
        }
        
        if let textForecast =  json["txt_forecast"]["forecastday"][(index*2)+1]["fcttext"].string {
          dayForecast.textForecastNight = textForecast
        }
        
        forecastDays.periods.append(dayForecast)
      }
    }
    
    
    DispatchQueue.main.async {
      if #available(iOS 10.0, *) {
        os_log("Callback to delegate %@ count %ul periods %ul", log: DataStore.dataStore_log, type:.debug,
               "didReceiveForecast",
               forecastDays.periods.count,forecastPeriods.periods.count )
      }
      
      self.delegate?.dataStore(self, didReceiveForecast: forecastDays, forecastPeriodsData:forecastPeriods)
    }
    
    
    for forecastPeriod in forecastPeriods.periods {
      if let icon = forecastPeriod.icon,
        let iconURLString = forecastPeriod.iconUrl {
        
        DispatchQueue.global().async {
          self.imageFor(icon, imageURLString: iconURLString)
        }
      }
    }
    
    for forecastDay in forecastDays.periods {
      if let icon = forecastDay.icon,
        let iconURLString = forecastDay.iconUrl {
        
        DispatchQueue.global().async {
          self.imageFor(icon, imageURLString: iconURLString)
        }
      }
    }
    
  }
  
  
  func weatherInfo(_ controller: WeatherInfo, didReceiveCurrentConditions json:JSON) {
    
    let conditionItemsUnsorted = Array(json.dictionaryValue.keys)
    let conditionItems = conditionItemsUnsorted.sorted{ $0 < $1 }.filter(){ (item) -> Bool in
      
      // Remove undesireables
      if item == "icon"
        || item == "icon_url"
        || item == "estimated"
      {
        return false
      }
      
      // Remove undesireable primary items
      
      if item == "temperature_string"
        || item == "weather"
        || item == "feelslike_string"
        || item == "station_id"
        || item == "wind_string"
        || item == "dewpoint_string"
        || item == "display_location"
      {
        return false
      }
      
      if item == "relative_humidity"
        || item == "UV"
        || item == "nowcast"
        || item == "image"
        || item == "solarradiation"
      {
        return false
      }
      
      // Skip on suffix
      for skipKey in ["_url",] {
        if item.hasSuffix(skipKey) {
          return false
        }
      }
      // Skip on prefix
      for skipKey in ["temp_","wind_","feelslike","visibility_","heat_","pressure_","precip_","observation_","local_","dewpoint_"] {
        if item.hasPrefix(skipKey) {
          return false
        }
      }
      
      if json[item].stringValue == "NA" {
        return false
      }
      
      //            if let valueText = json[item].string {
      //                if valueText == "NA" {
      //                    return false
      //                }
      //            }
      
      return true
    }
    
    //print(conditionItems)
    
    var currentConditions = CurrentConditionsData()
    
    for conditionItem in conditionItems {
      if let valueString = json[conditionItem].string {
        currentConditions.conditionItems.append((conditionItem,valueString))
      } else {
        if let valueNumber = json[conditionItem].double {
          currentConditions.conditionItems.append((conditionItem,"\(valueNumber)"))
        }
      }
    }
    
    currentConditions.weather = json["weather"].string
    
    if let cityName = json["display_location"]["city"].string,
      let stateName = json["display_location"]["state_name"].string ,
      let zipCode = json["display_location"]["zip"].string
    {
      delegate?.dataStore(self, primaryLocationTitle:"\(cityName), \(stateName) \(zipCode)")
    } else {
      delegate?.dataStore(self, primaryLocationTitle:"")
    }
    
    
    // primary items
    if let value = json["temperature_string"].string {
      currentConditions.primaryItems.append(("Temperature",value) )
    }
    if let value = json["feelslike_string"].string {
      currentConditions.primaryItems.append(("Feels Like",value) )
    }
    if let value = json["wind_string"].string {
      currentConditions.primaryItems.append(("Wind",value) )
    }
    if let value = json["dewpoint_string"].string {
      currentConditions.primaryItems.append(("Dewpoint",value) )
    }
    
    // secondary items
    if let value = json["UV"].string {
      currentConditions.secondaryItems.append(("UV",value) )
    }
    if let value =  json["feelslike_f"].string {
      currentConditions.secondaryItems.append(("Feels Like",value) )
    }
    if let value =  json["wind_dir"].string,
      let windSpeed =  json["wind_mph"].double
    {
      currentConditions.secondaryItems.append(("Wind","\(value) \(windSpeed) mph") )
    }
    if let value =  json["visibility_mi"].string {
      currentConditions.secondaryItems.append(("Visibility","\(value) mi") )
    }
    
    
    currentConditions.iconUrl = json["icon_url"].string
    
    if let tempf = json["temp_f"].int {
      currentConditions.temp = "\(tempf)"
    }
    
    //  print(currentConditions)
    
    DispatchQueue.main.async {
      self.delegate?.dataStore(self,didReceiveCurrentConditions:conditionItems, conditions:currentConditions)
    }
    
    //String
    if let icon = json["icon"].string,
      let iconURLString = json["icon_url"].string {
      DispatchQueue.global().async {
        self.imageFor(icon, imageURLString: iconURLString)
      }
      
      //Do something you want
    } else {
      //Print the error
      print(json["icon"])
      print(json["icon_url"])
    }
    
  }
    
  func weatherInfo(_ controller: WeatherInfo, didReceiveSattelite imageURLs:[String : AnyObject]) {
    // Empty impl
  }
  
  func weatherInfo(_ controller: WeatherInfo, didReceiveSatteliteImage image:UIImage) {
    delegate?.dataStore(self, didReceiveSatteliteImage:image)
  }
  
  
  
  // MARK:- Image methods
  
  /// Returns a UIImage for `iconName`.
  func imageFor(_ iconName:String) -> UIImage? {
    
    var result: UIImage? = nil
    if let cachedImage = availableImage(for: iconName) {
      result = cachedImage
    } else {
      imageFor("ondemand", imageURLString: iconName)
      result = imagePlaceholder
    }
    
    return result
  }
  
  /// Processes a UIImage for `iconName`; the `imageURLString` is the
  /// the location of the image
  func imageFor(_ iconName:String, imageURLString:String) -> Void {
    
    // The key for the cache must contain a notion of night vs day
    // icon = clear
    // iconURL = clear.gif or nt_clear.gif
    
    // Image needs retrieved when !pending && !available
    
    if imageNeedsRetrieved(for: imageURLString) {
      imageIsPending(for:imageURLString, iconName: iconName)
      
      DispatchQueue.global().async {
        var image:UIImage? = nil
        if let customIconSet = self.iconSet {
          // http //icons.wxug.com/i/c/a/partlycloudy.gif
          // http //icons.wxug.com/i/b/a/partlycloudy.gif
          if let imageURL = URL(string: imageURLString) {
            let modifiedIconURL = (imageURL as NSURL).deletingLastPathComponent?.deletingLastPathComponent().appendingPathComponent(customIconSet).appendingPathComponent(imageURL.lastPathComponent)
            
            if let imageIconURL = modifiedIconURL,
              let imageData = try? Data(contentsOf: imageIconURL) {
              image = UIImage(data: imageData)
            }
          }
          
        } else {
          if let imageURL = URL(string: imageURLString),
            let imageData = try? Data(contentsOf: imageURL) {
            image = UIImage(data: imageData)
          }
        }
        
        if let iconImage = image {
          self.imageBecameAvailable(iconImage, imageURLString: imageURLString)
          // Notify the delegate
          DispatchQueue.main.async {
            self.delegate?.dataStore(self, updateForIconImage:imageURLString)
          }
        }
      } // dispatch
    } // not pending
  }

  
  // Image needs retrieved when !pending && !available
  fileprivate func imageNeedsRetrieved(for imageKey: String) -> Bool {
    lock.lock(); defer { lock.unlock() }
    var result = false
    var pending = false
    var available = false
    if let _ = self.pendingImage[imageKey] {
      pending = true
    }
    if let _ = self.imageCache.object(forKey: imageKey as AnyObject) {
      available = true
    }
    
    result = !pending && !available
    return result
  }
  
  // available Image from image cache
  fileprivate func availableImage(for imageKey: String) -> UIImage? {
    lock.lock(); defer { lock.unlock() }
    return self.imageCache.object(forKey: imageKey as AnyObject) as? UIImage
  }
  
  // Became available, add to image cache, remove from pending
  fileprivate func imageBecameAvailable(_ iconImage: UIImage, imageURLString: String) {
    lock.lock(); defer { lock.unlock() }
    self.pendingImage.removeValue(forKey: imageURLString)
    self.imageCache.setObject(iconImage, forKey: imageURLString as AnyObject)
  }
  
  fileprivate func imageIsPending(for key:String, iconName: String) {
    lock.lock(); defer { lock.unlock() }
    self.pendingImage[key] = iconName
  }
  
}







/*
 JSON: {
 "current_observation" : {
 "precip_today_metric" : "0",
 "observation_time_rfc822" : "Sun, 18 Dec 2016 17:44:02 -0500",
 "feelslike_c" : "-6",
 "wind_mph" : 1.2,
 "precip_1hr_in" : "-999.00",
 "relative_humidity" : "100%",
 "dewpoint_f" : 20,
 "UV" : "0",
 "dewpoint_string" : "20 F (-6 C)",
 "local_tz_short" : "EST",
 "windchill_string" : "20 F (-6 C)",
 "temp_c" : -6.4,
 "heat_index_string" : "NA",
 "pressure_mb" : "1032",
 "pressure_in" : "30.47",
 "windchill_c" : "-6",
 "feelslike_string" : "20 F (-6 C)",
 "forecast_url" : "http:\/\/www.wunderground.com\/US\/MI\/Detroit.html",
 "heat_index_f" : "NA",
 "pressure_trend" : "+",
 "ob_url" : "http:\/\/www.wunderground.com\/cgi-bin\/findweather\/getForecast?query=42.338959,-83.037086",
 "temperature_string" : "20.5 F (-6.4 C)",
 "estimated" : {
 
 },
 "image" : {
 "url" : "http:\/\/icons.wxug.com\/graphics\/wu2\/logo_130x80.png",
 "title" : "Weather Underground",
 "link" : "http:\/\/www.wunderground.com"
 },
 "wind_degrees" : 35,
 "precip_today_string" : "0.00 in (0 mm)",
 "wind_gust_kph" : "5.0",
 "observation_epoch" : "1482101042",
 "weather" : "Mostly Cloudy",
 "heat_index_c" : "NA",
 "feelslike_f" : "20",
 "observation_location" : {
 "city" : "Lafayette Park, Detroit",
 "country" : "US",
 "longitude" : "-83.037086",
 "elevation" : "0 ft",
 "country_iso3166" : "US",
 "latitude" : "42.338959",
 "full" : "Lafayette Park, Detroit, Michigan",
 "state" : "Michigan"
 },
 "local_tz_offset" : "-0500",
 "precip_today_in" : "0.00",
 "local_time_rfc822" : "Sun, 18 Dec 2016 17:44:12 -0500",
 "solarradiation" : "--",
 "wind_string" : "From the NE at 1.2 MPH Gusting to 3.1 MPH",
 "history_url" : "http:\/\/www.wunderground.com\/weatherstation\/WXDailyHistory.asp?ID=KMIDETRO38",
 "windchill_f" : "20",
 "local_epoch" : "1482101052",
 "wind_gust_mph" : "3.1",
 "wind_kph" : 1.9,
 "display_location" : {
 "state" : "MI",
 "country_iso3166" : "US",
 "zip" : "48201",
 "full" : "Detroit, MI",
 "magic" : "1",
 "wmo" : "99999",
 "longitude" : "-83.05999756",
 "latitude" : "42.34999847",
 "elevation" : "188.1",
 "state_name" : "Michigan",
 "city" : "Detroit",
 "country" : "US"
 },
 "icon_url" : "http:\/\/icons.wxug.com\/i\/c\/k\/nt_mostlycloudy.gif",
 "precip_1hr_string" : "-999.00 in ( 0 mm)",
 "local_tz_long" : "America\/New_York",
 "temp_f" : 20.5,
 "observation_time" : "Last Updated on December 18, 5:44 PM EST",
 "icon" : "mostlycloudy",
 "precip_1hr_metric" : " 0",
 "dewpoint_c" : -6,
 "station_id" : "KMIDETRO38",
 "wind_dir" : "NE",
 "visibility_km" : "16.1",
 "visibility_mi" : "10.0",
 "nowcast" : ""
 },
 "response" : {
 "version" : "0.1",
 "termsofService" : "http:\/\/www.wunderground.com\/weather\/api\/d\/terms.html",
 "features" : {
 "conditions" : 1
 }
 }
 }
 ["windchill_c", "windchill_f", "windchill_string"]
 */


