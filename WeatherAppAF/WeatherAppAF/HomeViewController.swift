//
//  WAHomeViewController.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/28/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, HomeTableDelegate {
    
    /// UI Elements
    @IBOutlet weak var locationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationLabel.text = ""
    }
    
    // MARK: - WAHomeTableDelegate
    
    func homeTable(_ controller:HomeTableViewController, primaryLocationTitle title:String) {
        locationLabel.text = title
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WAEmbedHomeTableSegue" {
            if let homeTableViewController = segue.destination as? HomeTableViewController {
                homeTableViewController.delegate = self
            }
        }
    }
    
}
