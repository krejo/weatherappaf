//
//  WAWeatherServiceNS.swift
//  WeatherAppAF
//
//  Created by JOSEPH KERR on 9/7/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeatherServiceNS {
    
    let apiKey = "1dc7e22fb723f500"
    var currentCity = "Detroit"
    var currentState = "MI"
    let cacheFiles = CacheFiles()
    
    // MARK: - Private
    
    /**
     Performs retrieval of data from Service
     checks for cache file if responses already received and uses that
     by Serializing to JSON
     Otherwise the request is made to the server upon success
     writes the cache file
     
     processResponse method takes one argument
     response = serializedJSON response
     
     */
    
    func wuServiceRequest(_ service: String,
                          processResponse:@escaping (_ response:JSON) -> Void ) {
        
        wuServiceRequest(service, errorHandler: nil, processResponse: processResponse)
    }
    
    func wuServiceRequest(_ service: String,
                          errorHandler:((_ result:ServiceRequestError) -> Void)?,
                          processResponse:@escaping (_ response:JSON) -> Void ) {
        
        let urlString = "http://api.wunderground.com/api/\(apiKey)/\(service)/q/\(currentState)/\(currentCity).json"
        guard let wiURL = URL(string: urlString)
            else {
                print("Error Invalid URL \(urlString)")
                return
        }
        
        // Check to see if we have a valid cached file first
        
        if let cacheFileURL = URL.cacheFileURLFromURL(wiURL, delimiter: apiKey),
            let cacheResponse = cacheFiles.readCacheFile(cacheFileURL) {
            print("cache file Response")
            // Valid cache file, use it
            
            let json = JSON(data: cacheResponse)
            if json == JSON.null {
                print("Error processing JSON")
                errorHandler?(.serviceRequestErrorParse)
            } else {
                //print("JSON: \(json)")
                processResponse(json)
            }
            
        } else {
            
            // Not a valid cachefile, go to the server
            
            print("server Response")
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            
            let task = session.dataTask(with: wiURL, completionHandler: { data, response, error in
                
                // HTTP request assumes NSHTTPURLResponse force cast
                let httpResponse = response as! HTTPURLResponse
                print("HTTP Status Code = \(httpResponse.statusCode)")
                // if let response = response as? NSHTTPURLResponse where 200...299 ~= response.statusCode {
                if 200...299 ~= httpResponse.statusCode  {
                    
                    if let responseData = data , responseData.count > 0 {
                        
                        let json = JSON(data: responseData)
                        if json == JSON.null {
                            print("Error processing JSON")
                            errorHandler?(.serviceRequestErrorParse)
                        } else {
                            // Write the cache file
                            if let cacheFileURL = URL.cacheFileURLFromURL(wiURL, delimiter: self.apiKey) {
                                self.cacheFiles.writeCacheFile(cacheFileURL, data: responseData)
                            }
                            processResponse(json)
                        }
                        
                    } else {
                        errorHandler?(.serviceRequestErrorNoData)
                    }
                } // 200..299
                else {
                    errorHandler?(.serviceRequestErrorStatusCode)
                }
            })  // dataTaskWithURL completion
            
            task.resume()
        }
        
    }
    
    /**
     Obtains data from URL
     uses Response Data Handler .responseData
     */
    
    func wuDataRequest(_ urlString: String, errorHandler:((_ result:ServiceRequestError) -> Void)?,
                       processResponse:@escaping ((_ response:Data?) -> Void) ) {
        
        print("server Response")
        guard let wiURL = URL(string: urlString)
            else {
                print("Error Invalid URL \(urlString)")
                return
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let task = session.dataTask(with: wiURL, completionHandler: { data, response, error in
            
            // HTTP request assumes NSHTTPURLResponse force cast
            let httpResponse = response as! HTTPURLResponse
            print("HTTP Status Code = \(httpResponse.statusCode)")
            if 200...299 ~= httpResponse.statusCode  {
                
                processResponse(data)
                
            } // 200..299
            else {
                if let errorHandler = errorHandler {
                    errorHandler(.serviceRequestErrorStatusCode)
                }
            }
            
        })  // dataTaskWithURL completion
        
        task.resume()
    }
    
}
