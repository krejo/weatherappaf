//
//  WADayPageViewController.swift
//  WeatherAppAF
//
//  Created by JOSEPH KERR on 11/5/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

// Simply a wrapper for DayViewController
// has pageIndex and delegate that is passed on to DayViewController

import UIKit

class DayPageViewController: UIViewController {
    
    /// UI Elements
    @IBOutlet weak var containerView: UIView!
    
    /// Index to page for pageViewController
    var pageIndex: Int?
    
    /// Delegate day view info
    weak var delegate: DayViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.layer.masksToBounds = true
        containerView.layer.cornerRadius = 12.0
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WAEmbedDayView" {
            if let dayViewController = segue.destination as? DayViewController {
                dayViewController.pageIndex = pageIndex
                dayViewController.delegate = delegate
            }
        }
    }
    
}
