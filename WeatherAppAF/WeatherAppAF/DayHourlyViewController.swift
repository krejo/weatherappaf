//
//  WADayHourlyViewController.swift
//  WeatherAppAF
//
//  Created by JOSEPH KERR on 11/11/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

protocol DayHourlyViewControllerDelegate: class {
    func dayHourlyViewHourlyPeriods(_ controller: DayHourlyViewController?) -> HourPeriods?
}

public extension Notification.Name {
    public static let dayHourlyDataAvailalable = Notification.Name("hourlyDataAvailable")
}

/// Contains collectionView for hourly data

class DayHourlyViewController: UIViewController {
    
    /// UI Elements
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    /// Delegate to obtain hourly data for day
    weak var delegate: DayHourlyViewControllerDelegate?
    
    /// Collectionview delegate
    weak var hourlyCollectionDelegate: HourlyCollectionDataDelegate?
    
    /// Keep a reference from a presented collectionViewController
    weak var collectionView : UICollectionView?
    
    /// Datasource for hourly
    private var hourlyCollectionData = HourlyCollectionData()
    
    /// Used to determine refresh in progress
    private var refreshInProgress = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hourlyCollectionData.delegate = hourlyCollectionDelegate
        hourlyCollectionData.includeDOW = true
        hourlyCollectionData.useDetail = true
        containerView.isHidden = true
        
        NotificationCenter.default.addObserver(forName: .imageIconAvailable, object: nil, queue: nil) { (note) in
            if let userInfo = note.userInfo,
                let iconName = userInfo["icon"] as? String {
                self.updateCollectionForIconImage(iconName)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshData()
    }
    
    
    // MARK: -
    
    func refreshData() {
        refreshInProgress = true
        activity.startAnimating()
        
        if let hourlyData = self.delegate?.dayHourlyViewHourlyPeriods(self) {
            activity.stopAnimating()
            self.hourlyCollectionData.hourPeriods = hourlyData
            reloadCollection()
            refreshInProgress = false
        } else {
            /// Did not obtain data
            containerView.isHidden = true
            hourlyCollectionData.hourPeriods = nil
            collectionView?.reloadData()
            
            /// One time notification to be notified when data is available
            var obs : NSObjectProtocol?
            obs = NotificationCenter.default.addObserver(forName: .dayHourlyDataAvailalable, object: nil, queue: nil) { (note) in
                self.refreshInProgress = false
                
                if let hourlyData = self.delegate?.dayHourlyViewHourlyPeriods(self) {
                    self.hourlyCollectionData.hourPeriods = hourlyData
                    self.reloadCollection()
                }
                
                print(".dayHourlyDataAvailalable")
                NotificationCenter.default.removeObserver(obs!)
                self.activity.stopAnimating()
            }
        }
    }
    
    
    func reloadCollection() {
        collectionView?.reloadData()
        
        if let count = hourlyCollectionData.hourPeriods?.periods.count {
            if count < 23 {
                // Less than a full day must be current day
                let startIndex = 0
                let startIndexPath = IndexPath(item: startIndex, section: 0)
                collectionView?.scrollToItem(at: startIndexPath, at: .left, animated:false)
            } else {
                // Full day
                let startIndex = count / 2
                let startIndexPath = IndexPath(item: startIndex, section: 0)
                collectionView?.scrollToItem(at: startIndexPath, at: .centeredHorizontally, animated:false)
            }
        }
        
        containerView.isHidden = false
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WADayHourlyEmbedCollectionViewSegue" {
            if let collectionViewController = segue.destination as? UICollectionViewController {
                collectionView = collectionViewController.collectionView
                collectionView?.dataSource = hourlyCollectionData
                //collectionView?.delegate = hourlyCollectionData
            }
        }
    }
    
    
    // MARK: - Update Visible
    
    func updateCollectionForIconImage(_ iconName:String) {
        
        if let count = hourlyCollectionData.hourPeriods?.periods.count,
            let visible = collectionView?.indexPathsForVisibleItems {
            for indexPath in visible {
                if indexPath.item < count {
                    let hourItem = hourlyCollectionData.hourPeriods?.periods[indexPath.item]
                    if let iconURL = hourItem?.iconUrl , iconURL == iconName {
                        DispatchQueue.main.async {
                            self.collectionView?.reloadItems(at: [indexPath])
                        }
                    }
                }
            }
        }
    }
    
}

