//
//  WABrowseDayViewController.swift
//  WeatherAppAF
//
//  Created by JOSEPH KERR on 11/5/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

public extension Notification.Name {
    public static let imageIconAvailable = Notification.Name("imageIconAvailable")
}


/**
 Container for pageViewController.
 Primary delegate for data and paging information
 */

class BrowseDayViewController: UIViewController {
    
    /// UI Elements
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var containerPageViewController: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    /// Keep a reference to UIPageViewController
    weak var pageViewControl : UIPageViewController?
    
    /// Keep a reference to UIPageViewController
    fileprivate var pagingDataSource: DayPagingDatasource?
    
    /// Obtain weather data
    fileprivate var weatherDataStore = DataStore()
    
    /// Array of hour periods, each has array of hourData
    fileprivate var hourlyTenDayPeriods:[HourPeriods]?
    
    /// All day data for many days
    fileprivate var forecastDays:ForecastDays?
    
    /// Periods has an array periods, day and night included
    fileprivate var forecastedPeriods:ForecastPeriods?
    
    /// Timesatmp to determne auto refresh
    fileprivate var hourlyTimeStamp : Date?
    
    /// Determines whether refresh hourly in progress
    fileprivate var refreshHourlyInProgress = false
    
    /// Used to determines whether refresh in progress
    fileprivate var refreshInProgress = false
    
    /// Used to determine first appearance
    private var firstLoad = true
    
    /// Used to track last current page
    private var restoreCurrentPageIndex = 0
    
    
    // MARK: Lifecyclye
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherDataStore.delegate = self
        containerPageViewController.isHidden = true
        pageControl.isHidden = true
        NotificationCenter.default.addObserver(forName: .UIApplicationDidBecomeActive, object: nil, queue: nil, using: appFore)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("\(String(describing: self))" + #function)
        super.viewDidAppear(animated)
        if firstLoad {
            refreshData()
        }
        firstLoad = false
    }
    
    
    // MARK:
    
    func appFore(note: Notification) {
        print("\(String(describing: self))" + #function)
        restoreCurrentPageIndex = pageControl.currentPage
        refreshData()
    }
    
    
    /// Retrieves the forecast data and updates the UI on completion.
    private func refreshData() {
        if !refreshInProgress {
            refreshInProgress = true
            containerPageViewController.isHidden = true
            pageControl.isHidden = true
            activity.startAnimating()
            
            weatherDataStore.getForecastTen() { (result:ServiceRequestError) in
                // Errorhandler
                print(result)
                self.activity.stopAnimating()
                self.refreshInProgress = false
            }
        }
    }
    
    /// Retrieves the hourly data and updates the UI on completion.
    fileprivate func refreshHourly() {
        if !refreshHourlyInProgress {
            refreshHourlyInProgress = true
            hourlyTenDayPeriods = nil
            
            weatherDataStore.getHourlyTen() { (result:ServiceRequestError) in
                // Errorhandler
                print(result)
                self.refreshHourlyInProgress = false
            }
        }
    }
    
    fileprivate func validHourly() -> Bool {
        var result = false
        if let _ = hourlyTenDayPeriods,
            let timeStamp = hourlyTimeStamp {
            let timeSince = timeStamp.timeIntervalSinceNow
            if (-timeSince > 10) {
                print("Hourly Timedout")
            } else {
                // Still valid has not timed out
                result = true
            }
        }
        return result
    }
    
    
    func startBrowsing() {
        activity.stopAnimating()
        containerPageViewController.isHidden = false
        
        var startingIndex: Int
        if let count = forecastDays?.periods.count {
            pageControl.numberOfPages = count
            startingIndex = restoreCurrentPageIndex < count ? restoreCurrentPageIndex : 0
        } else {
            pageControl.numberOfPages = 0
            startingIndex = 0
        }
        
        pageControl.isHidden = false
        pagingDataSource = DayPagingDatasource()
        pagingDataSource?.delegate = self
        pagingDataSource?.dayViewDelegate = self
        pageViewControl?.delegate = self
        pageViewControl?.dataSource = pagingDataSource
        pageViewControl?.view.backgroundColor = self.view.backgroundColor
        
        //let startingIndex = restoreCurrentPageIndex < self.forecastDaysData.count ? restoreCurrentPageIndex : 0
        
        let startingViewController = pagingDataSource?.dayPage(at: startingIndex)
        startingViewController?.view.backgroundColor = pageViewControl?.view.backgroundColor
        
        pageControl.currentPage = startingIndex
        
        pageViewControl?.setViewControllers([startingViewController!], direction: .forward, animated: false, completion: nil)
    }
    
    // MARK: Helpers
    
    // Item Data
    // DayData [String:AnyObject]
    // DayForecastData [String:AnyObject]
    // NiteForecastData [String:AnyObject]
    //
    //      dayIndex 0
    //         DaysData [0]
    //         ForecastPeriods [0]
    //         ForecastPeriods [1]
    //      dayIndex 1
    //         DaysData [1]
    //         ForecastPeriods [2]
    //         ForecastPeriods [3]
    //      dayIndex 2
    //         DaysData [1]
    //         ForecastPeriods [4]
    //         ForecastPeriods [5]
    
    func dayItem(at index:Int) -> [String : AnyObject]? {
        var result: [String : AnyObject]?
        
        if let count = forecastDays?.periods.count, index < count {
            let periodIndex1 = index*2
            let periodIndex2 = periodIndex1 + 1
            result = [:]  // Create the dictionary
            result?["DayData"] = forecastDays?.periods[index] as AnyObject?
            result?["ForecastDataDay"] = forecastedPeriods?.periods[periodIndex1] as AnyObject?
            result?["ForecastDataNight"] = forecastedPeriods?.periods[periodIndex2] as AnyObject?
        }
        
        return result
    }
    
    func imageForDayItem(at index:Int) -> UIImage? {
        
        var result : UIImage?
        
        if let forecastData = dayItem(at: index),
            let dayData = forecastData["DayData"] as? ForecastDay,
            let iconURL = dayData.iconUrl {
            result = self.weatherDataStore.imageFor(iconURL)
        }
        
        return result
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WAEmbedPageViewController" {
            if let pageViewController = segue.destination as? UIPageViewController {
                self.pageViewControl = pageViewController
            }
        }
    }
    
}



// MARK: - pageViewController delegate

extension BrowseDayViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        //Called after a gesture-driven transition completes.
        if completed {
            if let dayPageViewController = pageViewController.viewControllers?[0] as? DayPageViewController,
                let pageIndex = dayPageViewController.pageIndex {
                pageControl.currentPage = pageIndex
            }
        }
    }
}

// MARK: Paging datasource Delegate

extension BrowseDayViewController: DayPagingDelegate {
    
    func paging(_ controller: DayPagingDatasource?, itemAtIndex index:Int) -> [String : AnyObject]? {
        return dayItem(at: index)
    }
    
    func pagingNumberOfPages(_ controller: DayPagingDatasource?) -> Int {
        if let count = forecastDays?.periods.count {
            return count
        }
        return 0
    }
    
    func paging(_ controller: DayPagingDatasource?, imageForItemAtIndex index:Int) -> UIImage? {
        return imageForDayItem(at: index)
    }
    
}

// MARK: DayView delegate

extension BrowseDayViewController: DayViewControllerDelegate {
    
    func dayView(_ controller: DayViewController?, itemAtIndex index:Int) -> [String:AnyObject]? {
        return dayItem(at: index)
    }
    
    func dayView(_ controller: DayViewController?, imageForItemAtIndex index:Int) -> UIImage? {
        return imageForDayItem(at: index)
    }
    
    func dayView(_ controller: DayViewController?, notificationNameForItemAt index:Int) -> Notification.Name? {
        var result : Notification.Name?
        
        if let forecastData = dayItem(at: index) {
            if let dayData = forecastData["DayData"] as? ForecastDay,
                let iconURL = dayData.iconUrl {
                result = Notification.Name(iconURL)
            }
        }
        return result
    }
    
    func dayView(_ controller: DayViewController?, hourlyDataPeriodsForDayAt index:Int) -> HourPeriods? {
        if validHourly() {
            if let hourPeriods = hourlyTenDayPeriods?[index] {
                return hourPeriods
            }
            // return hourlyTenPeriods?[index]
        } else {
            refreshHourly()
        }
        return nil
    }
    
    func dayView(_ controller: DayViewController?, imageFor iconName:String) -> UIImage? {
        return weatherDataStore.imageFor(iconName)
    }
    
}

// MARK: WADataStoreDelegate

extension BrowseDayViewController: DataStoreDelegate {
    
    func dataStore(_ controller: DataStore, didReceiveForecast forecastDayData:ForecastDays, forecastPeriodsData:ForecastPeriods )
    {
        print(#function)
        forecastedPeriods = forecastPeriodsData // 20 items day and night
        forecastDays = forecastDayData // 10 items conditions high low
        refreshInProgress = false
        startBrowsing()
    }
    
    func dataStore(_ controller: DataStore, didReceiveHourlyTen hourPeriods:[HourPeriods]) {
        hourlyTenDayPeriods = hourPeriods
        refreshHourlyInProgress = false
        hourlyTimeStamp = Date()
        NotificationCenter.default.post(name: .dayHourlyDataAvailalable, object: self)
    }
    
    func dataStore(_ controller: DataStore, updateForIconImage iconName:String) {
        let noti = Notification(name: Notification.Name(iconName))
        NotificationCenter.default.post(noti)
        let notiImageAvailable = Notification(name: .imageIconAvailable, object: self, userInfo: ["icon" : iconName])
        NotificationCenter.default.post(notiImageAvailable)
    }
    
}
