//
//  WAWeatherServiceAF.swift
//  WeatherAppAF
//
//  Created by JOSEPH KERR on 9/4/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WeatherServiceAF {
    
    let apiKey = "1dc7e22fb723f500"
    var currentCity = "Detroit"
    var currentState = "MI"
    let cacheFiles = CacheFiles()
    
    // MARK: - Private
    
    /**
     Performs retrieval of data from Service
     checks for cache file if responses already received and uses that
     by Serializing to JSON
     Otherwise the request is made to the server upon success
     writes the cache file
     
     Uses Response JSON Handler .responseJSON
     
     processResponse method takes one argument
     response = response.result.value  parsed data as JSON as [String:AnyObject]
     
     */
    
    func wuServiceRequest(_ service: String, processResponse:@escaping (_ response:JSON) -> Void ) {
        
        wuServiceRequest(service, errorHandler: nil, processResponse: processResponse)
    }
    
    func wuServiceRequest(_ service: String,
                          errorHandler:((_ result:ServiceRequestError) -> Void)?,
                          processResponse:@escaping (_ response:JSON) -> Void ) {
        /*
         func wuServiceRequest(_ service: String,
         errorHandler:((_ result:WAServiceRequestError) -> Void)?,
         processResponse:@escaping (_ response:AnyObject?) -> Void ) {
         */
        
        let urlString = "http://api.wunderground.com/api/\(apiKey)/\(service)/q/\(currentState)/\(currentCity).json"
        guard let wiURL = URL(string: urlString)
            else {
                print("Error Invalid URL \(urlString)")
                return
        }
        
        // Check to see if we have a valid cached file first
        
        if let cacheFileURL = URL.cacheFileURLFromURL(wiURL, delimiter: apiKey),
            let cacheResponse = cacheFiles.readCacheFile(cacheFileURL) {
            print("cache file Response")
            // Valid cache file, use it
            let json = JSON(data: cacheResponse)
            if json == JSON.null {
                print("Error processing JSON")
                errorHandler?(.serviceRequestErrorParse)
            } else {
                //print("JSON: \(json)")
                processResponse(json)
            }
            
            
        } else {
            
            // Not a valid cachefile, go to the server
            print("server Response")
            
            
            let _ = Alamofire.request(urlString, method: .get)
                .validate()
                .responseJSON { response in
                    print("\(response.request!.httpMethod!) \(response.request!) (\(response.response!.statusCode))")
                    
                    switch response.result {
                    case .success(let value):
                        print("Validation Successful")
                        
                        if let cacheFileURL = URL.cacheFileURLFromURL(wiURL, delimiter: self.apiKey),
                            let responseData = response.data {
                            self.cacheFiles.writeCacheFile(cacheFileURL, data: responseData)
                        }
                        
                        let json = JSON(value)
                        //print("JSON: \(json)")
                        processResponse(json)
                        
                        
                    case .failure(let error):
                        print(error)
                        var errorResult : ServiceRequestError = .serviceRequestErrorUnknown
                        if let error = error as? AFError, error.isResponseValidationError,
                            let _ = error.responseCode {
                            errorResult = .serviceRequestErrorStatusCode
                        }
                        
                        if let error = error as? AFError, error.isResponseSerializationError {
                            errorResult = .serviceRequestErrorParse
                        }
                        errorHandler?(errorResult)
                    }
            }
            
            // debugPrint(r)
        }
        
    }
    
    /**
     Obtains data from URL
     uses Response Data Handler .responseData
     */
    
    func wuDataRequest(_ urlString: String, errorHandler:((_ result:ServiceRequestError) -> Void)?, processResponse:@escaping ((_ response:Data?) -> Void) ) {
        
        print("server Response")
        
        Alamofire.request(urlString, method: .get)
            .validate()
            .responseData { response in
                
                switch response.result {
                case .success:
                    if let jsonResponseData = response.result.value {
                        // Make the completion call
                        processResponse(jsonResponseData)
                    } else {
                        errorHandler?(.serviceRequestErrorNoData)
                    }
                    
                case .failure(let error):
                    print(error)
                    var errorResult : ServiceRequestError = .serviceRequestErrorUnknown
                    
                    if let error = error as? AFError, error.isResponseValidationError,
                        let _ = error.responseCode {
                        errorResult = .serviceRequestErrorStatusCode
                    }
                    
                    errorHandler?(errorResult)
                }
        }
        
    }
    
}


// A verbose implementation of error handling, not in use but wanted
// for reference

extension WeatherServiceAF {
    fileprivate func checkError(_ result: Result<Data>) -> Bool {
        
        guard case let .failure(error) = result else { return false }
        
        if let error = error as? AFError {
            switch error {
            case .invalidURL(let url):
                print("Invalid URL: \(url) - \(error.localizedDescription)")
            case .parameterEncodingFailed(let reason):
                print("Parameter encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
            case .multipartEncodingFailed(let reason):
                print("Multipart encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
            case .responseValidationFailed(let reason):
                print("Response validation failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    print("Downloaded file could not be read")
                case .missingContentType(let acceptableContentTypes):
                    print("Content Type Missing: \(acceptableContentTypes)")
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                case .unacceptableStatusCode(let code):
                    print("Response status code was unacceptable: \(code)")
                }
            case .responseSerializationFailed(let reason):
                print("Response serialization failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
            }
            
            print("Underlying error: \(error.underlyingError)")
        } else if let error = error as? URLError {
            print("URLError occurred: \(error)")
        } else {
            print("Unknown error: \(error)")
        }
        
        return true
    }
    
}


//if let error = error as? AFError {
//    switch error {
//    case .responseValidationFailed(let reason):
//        print("Response validation failed: \(error.localizedDescription)")
//        print("Failure Reason: \(reason)")
//        switch reason {
//        case .unacceptableStatusCode(let code):
//            errorResult = .waServiceRequestErrorStatusCode
//            print("Response status code was unacceptable: \(code)")
//        default:
//            break
//        }
//    default:
//        break
//    }
//}


// .responseJSON { response in
//                    print(response.request)  // original URL request
//                    print(response.response) // HTTP URL response
//                    print(response.data)     // server data
//                    print(response.result)   // result of response serialization
