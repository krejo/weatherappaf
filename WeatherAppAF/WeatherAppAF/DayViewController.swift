//
//  WADayViewController.swift
//  WeatherAppAF
//
//  Created by JOSEPH KERR on 11/5/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

/**
 The DayViewController delegate to obtain its data
 */
protocol DayViewControllerDelegate: class {
    func dayView(_ controller: DayViewController?, itemAtIndex index:Int) -> [String:AnyObject]?
    func dayView(_ controller: DayViewController?, imageForItemAtIndex index:Int) -> UIImage?
    func dayView(_ controller: DayViewController?, notificationNameForItemAt index:Int) -> Notification.Name?
    func dayView(_ controller: DayViewController?, imageFor iconName:String) -> UIImage?
    func dayView(_ controller: DayViewController?, hourlyDataPeriodsForDayAt index:Int) -> HourPeriods?
}

/**
 Manages the presentation of one day data. Data supplied by it's delegate.
 Is itself a delegate for hourly data
 */

class DayViewController: UIViewController {
    
    /// UI Elements
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var primaryImageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var amountsLabel: UILabel!
    @IBOutlet weak var conditionsLabel: UILabel!
    
    /// A delegate to obtain information about the days weather
    weak var delegate : DayViewControllerDelegate?
    
    /// A page index for embedding pageviewController
    var pageIndex: Int?
    
    /// A Collectionview datasource
    private var hourlyCollectionData = HourlyCollectionData()
    
    /// Keeps a reference to a presentationController
    fileprivate var customPresentationController: PresentationController?
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hourlyCollectionData.delegate = self
        refreshDayData()
    }
    
    // MARK:
    
    /// Obtains the day data using the `delegate` and refershes the UI.
    private func refreshDayData() {
        
        if let pageIndex = pageIndex {
            primaryImageView.image = delegate?.dayView(self, imageForItemAtIndex: pageIndex)
            
            if let notiName = delegate?.dayView(self, notificationNameForItemAt: pageIndex) {
                /// One time notification
                var token : NSObjectProtocol?
                token = NotificationCenter.default.addObserver(forName: notiName, object: nil, queue: nil) { (note) in
                    print(notiName.rawValue)
                    self.primaryImageView.image = self.delegate?.dayView(self, imageForItemAtIndex: pageIndex)
                    NotificationCenter.default.removeObserver(token!)
                }
            }
            
            if let forecastData = delegate?.dayView(self, itemAtIndex: pageIndex) {
                var primaryText = ""
                var detailText = ""
                
                if let forecastDataDay = forecastData["ForecastDataDay"] as? ForecastPeriod {
                    if let titleText = forecastDataDay.title {
                        primaryText = titleText
                    }
                    
                    if let forecastText = forecastDataDay.text {
                        detailText += "Day: " + forecastText
                    }
                }
                
                if let forecastDataNight = forecastData["ForecastDataNight"] as? ForecastPeriod {
                    if let forecastText = forecastDataNight.text {
                        detailText += "\n\n"
                        detailText += "Night: " + forecastText
                    }
                }
                
                if let dayData = forecastData["DayData"] as? ForecastDay {
                    
                    if let conditions = dayData.conditions {
                        conditionsLabel.text = conditions
                    }
                    
                    // Possible Precipitation
                    var precipString = ""
                    if let pop = dayData.possiblePrecipitation, pop > 50 {
                        let possiblePrecip = "\(pop)%"
                        precipString += possiblePrecip
                        print(precipString)
                    }
                    
                    var precipAmount = 0.0
                    if let qpfDay = dayData.quantityPrecipAmountDay {
                        precipAmount += qpfDay
                    }
                    
                    if let qpfNight = dayData.quantityPrecipAmountNight {
                        precipAmount += qpfNight
                    }
                    
                    if precipAmount > 0.001 {
                        let precipAmount = "\(precipAmount) inches"
                        if !precipString.isEmpty {
                            precipString += "\n"
                        }
                        precipString += precipAmount
                    }
                    
                    
                    if precipString.isEmpty {
                        amountsLabel.isHidden = true
                    } else {
                        amountsLabel.text = precipString
                        amountsLabel.isHidden = false
                    }
                    
                    if let _ = dayData.tempHigh {
                        primaryText += "  \(dayData.tempHighString)"
                    }
                    
                    if let _ = dayData.tempLow {
                        primaryText += "  \(dayData.tempLowString)"
                    }
                }
                
                textView.text = detailText
                titleLabel.text = primaryText
            }
        }
    }
    
    
    // MARK: Tap request
    
    @IBAction func tapView(_ sender: Any) {
        presentHourlyView()
    }
    
}


// MARK: - Present hourly view controller

extension DayViewController {
    
    func presentHourlyView() {
        
        if let hourlyViewController = self.storyboard?.instantiateViewController(withIdentifier: "WADayHourlyView") as? DayHourlyViewController {
            hourlyViewController.delegate = self
            hourlyViewController.hourlyCollectionDelegate = self
            
            hourlyViewController.preferredContentSize = CGSize(width: 0, height: 140)
            hourlyViewController.view.backgroundColor = UIColor.white.withAlphaComponent(0.35)
            
            self.customPresentationController =
                PresentationController(presentedViewController:hourlyViewController, presenting: self)
            
            customPresentationController?.useRounded = true
            
            hourlyViewController.transitioningDelegate = customPresentationController
            present(hourlyViewController, animated:true, completion:nil)
        }
    }
}

// MARK: WAHourlyCollectionDataDelegate

extension DayViewController: HourlyCollectionDataDelegate {
    
    func hourlyCollection(_ controller:HourlyCollectionData, imageForIcon iconName:String) -> UIImage? {
        return delegate?.dayView(self, imageFor: iconName)
    }
    
}

// MARK: WADayHourlyViewControllerDelegate

extension DayViewController: DayHourlyViewControllerDelegate {
    
    func dayHourlyViewHourlyPeriods(_ controller: DayHourlyViewController?) -> HourPeriods? {
        if let pageIndex = pageIndex,
            let hourlyDayData = delegate?.dayView(self, hourlyDataPeriodsForDayAt:pageIndex) {
            return hourlyDayData
        }
        return nil
    }
    
}

