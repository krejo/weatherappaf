//
//  WAForecastTenTableViewController.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/22/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class ForecastTenTableViewController: ForecastTableViewController {
    
    override func refreshData() {
        
        //weatherInfo.getForecastTen()
        
        weatherDataStore.getForecastTen() { (result:ServiceRequestError) in
            // ErrorHandler
            print(result)
            self.refreshInProgress = false
            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
            }
        }
    }
}
