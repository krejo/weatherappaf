//
//  WAHourlyViewController.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/24/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class HourlyViewController: UIViewController {
    
    /// UI Elements
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    /// Obtain weather data
    var weatherDataStore = DataStore()
    
    /// Reference to embedded collection view
    weak var collectionView : UICollectionView?
    
    /// Datasource for hourly
    fileprivate var hourlyCollectionData = HourlyCollectionData()
    
    /// Determin refresh in progress
    fileprivate var refreshInProgress = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherDataStore.delegate = self
        hourlyCollectionData.delegate = self
        hourlyCollectionData.includeDOW = true
        hourlyCollectionData.useDetail = true
        containerView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshData()
    }
    
    // MARK: -
    
    func refreshData() {
        refreshInProgress = true
        activity.startAnimating()
        
        weatherDataStore.getHourly() { (result:ServiceRequestError) in
            // Errorhandler
            print(result)
            self.refreshInProgress = false
            self.activity.stopAnimating()
        }
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "WAEmbedCollectionViewSegue" {
            if let collectionViewController = segue.destination as? UICollectionViewController {
                collectionView = collectionViewController.collectionView
                collectionView?.dataSource = hourlyCollectionData
                //collectionView?.delegate = hourlyCollectionData
            }
        }
    }
    
    
    // MARK: - Update Visible
    
    func updateCollectionForIconImage(_ iconName:String) {
        
        if let count = hourlyCollectionData.hourPeriods?.periods.count,
            let visible = collectionView?.indexPathsForVisibleItems {
            
            for indexPath in visible {
                if indexPath.item < count {
                    let hourItem = hourlyCollectionData.hourPeriods?.periods[indexPath.item]
                    if let iconURL = hourItem?.iconUrl , iconURL == iconName {
                        DispatchQueue.main.async {
                            self.collectionView?.reloadItems(at: [indexPath])
                        }
                    }
                }
                
            } // for indexPath
            
        } // let visible count
    }
    
}


// MARK: - WADataStoreDelegate

extension HourlyViewController: DataStoreDelegate {
    
    func dataStore(_ controller: DataStore, updateForIconImage iconName:String) {
        updateCollectionForIconImage(iconName)
    }
    
    func dataStore(_ controller: DataStore, didReceiveHourly hourPeriods:HourPeriods) {
        //refreshHourlyInProgress = false
        activity.stopAnimating()
        self.refreshInProgress = false
        hourlyCollectionData.hourPeriods = hourPeriods
        collectionView?.reloadData()
        containerView.isHidden = false
    }
}

// Mark: WAHourlyCollectionDataDelegate

extension HourlyViewController: HourlyCollectionDataDelegate {
    
    func hourlyCollection(_ controller:HourlyCollectionData, imageForIcon iconName:String) -> UIImage? {
        return self.weatherDataStore.imageFor(iconName)
    }
    
}


