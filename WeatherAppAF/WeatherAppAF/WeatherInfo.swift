//
//  WAWeatherInfo.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/21/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

protocol WeatherInfoDelegate : class {
    func weatherInfo(_ controller: WeatherInfo, didReceiveCurrentConditions conditions:JSON)
    func weatherInfo(_ controller: WeatherInfo, didReceiveForecast forecast:JSON)
    func weatherInfo(_ controller: WeatherInfo, didReceiveHourly hourPeriods:JSON)
    func weatherInfo(_ controller: WeatherInfo, didReceiveHourlyTen hourTenPeriods:JSON)
    func weatherInfo(_ controller: WeatherInfo, didReceiveSatteliteImage image:UIImage)
    func weatherInfo(_ controller: WeatherInfo, didReceiveSattelite imageURLs:[String : AnyObject])
}

extension WeatherInfoDelegate {
    func weatherInfo(_ controller: WeatherInfo, didReceiveForecast forecast:JSON){}
    func weatherInfo(_ controller: WeatherInfo, didReceiveHourly hourPeriods:JSON){}
    func weatherInfo(_ controller: WeatherInfo, didReceiveHourlyTen hourTenPeriods:JSON){}
}


enum ServiceRequestError : Error {
    case serviceRequestErrorParse
    case serviceRequestErrorStatusCode
    case serviceRequestErrorUnknown
    case serviceRequestErrorDataNotFound(dataItem:String)
    case serviceRequestErrorNoData
}


public struct Services {
    public struct Names {
        public static let Conditions = "conditions"
        public static let Hourly = "hourly"
        public static let HourlyTenDay = "hourly10day"
        public static let Forecast = "forecast"
        public static let ForecastTenDay = "forecast10day"
        public static let Satellite = "satellite"
    }
    
    public struct ResponseFieldNames {
        public static let CurrentObservation = "current_observation"
        public static let HourlyForecast = "hourly_forecast"
        public static let Forecast = "forecast"
        public static let Satellite = "satellite"
        public static let ImageURLvis = "image_url_vis"
    }
}


/**
 WAWeatherInfo is object that performs retrieval of weather info
 JSON returned on the service call is lightly parsed for specific key fields
 And that structure is returned
 
 */

class WeatherInfo: WeatherServiceAF {
    
    /// A delegate to inform about results.
    weak var delegate: WeatherInfoDelegate?
    
    // MARK: - Public API
    
    /// Submits the request for Current Conditions; calls `errorHandler` on error;
    /// Calls `delegate` method with desired results.
    func getCurrentConditions (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
        
        wuServiceRequest(Services.Names.Conditions, errorHandler: errorHandler) { json in
            
            if json[Services.ResponseFieldNames.CurrentObservation].exists() {
                self.delegate?.weatherInfo(self, didReceiveCurrentConditions:json[Services.ResponseFieldNames.CurrentObservation])
            } else {
                errorHandler?(.serviceRequestErrorDataNotFound(dataItem: Services.ResponseFieldNames.CurrentObservation))
            }
        }
    }
    /// Submits the request for Hourly data; calls `errorHandler` on error;
    /// Calls `delegate` method with desired results.
    func getHourly (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
        
        wuServiceRequest(Services.Names.Hourly, errorHandler: errorHandler) { json in
            
            if json[Services.ResponseFieldNames.HourlyForecast].exists() {
                let hourly = json[Services.ResponseFieldNames.HourlyForecast]
                self.delegate?.weatherInfo(self, didReceiveHourly:hourly)
            } else {
                errorHandler?(.serviceRequestErrorDataNotFound(dataItem: Services.ResponseFieldNames.HourlyForecast))
            }
        }
        
    }
    /// Submits the request for Hourly data; calls `errorHandler` on error;
    /// Calls `delegate` method with desired results.
    func getHourlyTen (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
        
        wuServiceRequest(Services.Names.HourlyTenDay, errorHandler: errorHandler) { json in
            
            if json[Services.ResponseFieldNames.HourlyForecast].exists() {
                let hourly = json[Services.ResponseFieldNames.HourlyForecast]
                self.delegate?.weatherInfo(self, didReceiveHourlyTen:hourly)
            } else {
                errorHandler?(.serviceRequestErrorDataNotFound(dataItem: Services.ResponseFieldNames.HourlyForecast))
            }
        }
        
    }
    
    func getForecast (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
        getForecast(with: Services.Names.Forecast, errorHandler: errorHandler)
    }
    
    
    func getForecastTen (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
        getForecast(with: Services.Names.ForecastTenDay, errorHandler: errorHandler)
    }
    
    
    fileprivate func getForecast (with service: String, errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
        
        wuServiceRequest(service, errorHandler: errorHandler) { json in
            
            if json[Services.ResponseFieldNames.Forecast].exists() {
                let forecast = json[Services.ResponseFieldNames.Forecast]
                self.delegate?.weatherInfo(self, didReceiveForecast:forecast)
            } else {
                errorHandler?(.serviceRequestErrorDataNotFound(dataItem: Services.ResponseFieldNames.Forecast))
            }
        }
    }
    
    
    func getSattelite (_ errorHandler:((_ result:ServiceRequestError) -> Void)? ) {
        
        wuServiceRequest(Services.Names.Satellite, errorHandler: errorHandler) { json in
            
            if let imageUrlString = json[Services.ResponseFieldNames.Satellite][Services.ResponseFieldNames.ImageURLvis].string {
                let imageRequestUrlString = imageUrlString + self.apiKey
                self.getSatteliteImageAtURL(imageRequestUrlString, errorHandler:errorHandler)
            } else {
                errorHandler?(.serviceRequestErrorDataNotFound(dataItem: Services.ResponseFieldNames.Satellite))
            }
        }
    }
    
    func getSatteliteImageAtURL (_ urlString: String, errorHandler:((_ result:ServiceRequestError) -> Void)?) {
        
        wuDataRequest(urlString, errorHandler: errorHandler) { responseData in
            
            if let imageData = responseData,
                let satImage = UIImage(data: imageData) {
                
                self.delegate?.weatherInfo(self, didReceiveSatteliteImage: satImage)
            } else {
                
                // Cannot convert retrieved data to UIImage
                errorHandler?(.serviceRequestErrorNoData)
            }
        }
    }
    
    // MARK: Convenience without errorHandling
    
    func getCurrentConditions () {
        getCurrentConditions(nil)
    }
    
    func getHourly () {
        getHourly(nil)
    }
    
    func getHourlyTen () {
        getHourlyTen(nil)
    }
    
    func getForecast () {
        getForecast(nil)
    }
    
    func getForecastTen () {
        getForecastTen(nil)
    }
    
    func getSattelite () {
        getSattelite(nil)
    }
    
    func getSatteliteImageAtURL (_ urlString: String) {
        getSatteliteImageAtURL(urlString, errorHandler: nil)
    }
    
    func getForecastWith (_ service: String) {
        getForecast(with: service, errorHandler: nil)
    }
    
}




