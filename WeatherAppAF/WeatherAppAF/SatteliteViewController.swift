//
//  WASatteliteViewController.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/21/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class SatteliteViewController: UIViewController, DataStoreDelegate {
    
    /// UI Elements
    @IBOutlet weak var imageView: UIImageView!
    
    /// Weather info data
    var weatherDataStore = DataStore()
    
    /// Determine on first appearance
    fileprivate var firstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        weatherDataStore.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if firstLoad {
            weatherDataStore.getSatellite()
        }
        firstLoad = false
    }
    
    
    // Mark: WADataStoreDelegate
    
    func dataStore(_ controller: DataStore, didReceiveSatteliteImage image:UIImage) {
        DispatchQueue.main.async {
            self.imageView.image = image
        }
    }
    
}
