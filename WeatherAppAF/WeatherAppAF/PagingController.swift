//
//  File.swift
//  WeatherAppAF
//
//  Created by JOSEPH KERR on 11/5/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//


import UIKit

protocol DayPagingDelegate: class {
    func paging(_ controller: DayPagingDatasource?, itemAtIndex index:Int) -> [String:AnyObject]?
    func pagingNumberOfPages(_ controller: DayPagingDatasource?) -> Int
    func paging(_ controller: DayPagingDatasource?, imageForItemAtIndex index:Int) -> UIImage?
}

/**
 WADayPagingDatasource conforms to UIPageViewControllerDataSource
 
 It has a delegate to obtain various bits of information
 
 */

class DayPagingDatasource: NSObject, UIPageViewControllerDataSource {
    
    weak var delegate: DayPagingDelegate?
    weak var dayViewDelegate: DayViewControllerDelegate?
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let dayPageViewController = viewController as? DayPageViewController,
            let sourcePage = dayPageViewController.pageIndex,
            sourcePage > 0
            else {
                return nil
        }
        
        if let result = dayPage(at: sourcePage - 1) {
            result.view.backgroundColor = pageViewController.view.backgroundColor
            return result
        } else {
            return nil
        }

    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let numberOfPages = delegate?.pagingNumberOfPages(self),
            let dayPageViewController = viewController as? DayPageViewController,
            let sourcePage = dayPageViewController.pageIndex,
            sourcePage < numberOfPages - 1
            else {
                return nil
        }
        
        if let result = dayPage(at: sourcePage + 1) {
            result.view.backgroundColor = pageViewController.view.backgroundColor
            return result
        } else {
            return nil
        }
        
    }
    
    func dayPage(at index: Int) -> UIViewController? {
        
        guard let dayViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "WADayPage") as? DayPageViewController
            else {
                return nil
        }
        
        dayViewController.delegate = dayViewDelegate
        dayViewController.pageIndex = index
        return dayViewController
    }
    
}



