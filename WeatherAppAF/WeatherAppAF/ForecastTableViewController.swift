//
//  WAForecastTableViewController.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/21/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class ForecastRevealCell: UITableViewCell {
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
}


class ForecastTableViewController: UITableViewController {
    
    /// Obtain weather data
    var weatherDataStore = DataStore()
    
    /// Array of hour periods, each has array of hourData
    var hourlyTenPeriods:[HourPeriods]?
    
    /// All day data for many days
    var forecastDays:ForecastDays?
    
    /// Periods has an array periods, day and night included
    var forecastedPeriods:ForecastPeriods?
    
    /// Collectionview datasource for hourly
    var hourlyCollectionData = HourlyCollectionData()
    
    /// Used to determine whether refresh in progress
    var refreshInProgress = false
    
    /// Used to determine whether refresh in progress
    var refreshForecastInProgress = false
    
    /// Used to determine whether refresh in progress
    var refreshHourlyInProgress = false
    
    /// Track the revealed row
    var revealHourlyRow: Int?  // Contains the index row of the revealed cell
    
    /// Used to determine first appearance
    var firstLoad = true
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        weatherDataStore.delegate = self
        hourlyCollectionData.delegate = self
        self.clearsSelectionOnViewWillAppear = false
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action:#selector(refreshTable(_:)), for:[.valueChanged])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("\(String(describing: self)) \(#function)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if firstLoad {
            DispatchQueue.main.async {
                self.refreshTable(nil)
            }
        } else {
            self.tableView.reloadData()
        }
        firstLoad = false
    }
    
    // MARK: -
    
    func refreshTable(_ control:AnyObject?) {
        if !refreshInProgress {
            if control == nil {
                // Programmatically started
                self.refreshControl?.beginRefreshing()
                refreshInProgress = true
                refreshData()
            } else {
                dismissHourlyCell(false)
                hourlyTenPeriods = nil
                hourlyCollectionData.hourPeriods = nil
                //hourlyCollectionData.hourlyPeriods = []
                refreshInProgress = true
                refreshData()
            }
        }
    }
    
    func refreshData() {
        weatherDataStore.getForecast() { (result:ServiceRequestError) in
            // Errorhandler
            print(result)
            self.refreshInProgress = false
            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    // MARK: Helper
    
    func selectPeriod() {
        if let revealIndex = revealHourlyRow {
            var dayIndex = 0
            dayIndex = revealIndex / 2
            if let hourlyTenItems = hourlyTenPeriods {
                hourlyCollectionData.hourPeriods = hourlyTenItems[dayIndex]
            }
        }
    }
    
    func isNightForRow(_ index:Int) -> Bool {
        var result = false
        if let forecastPeriod = forecastedPeriods?.periods[index] {
            if let icon = forecastPeriod.icon {
                result = icon.hasPrefix("nt_")
            }
        }
        
        return result
    }
    
    
    // MARK:- UITableview Reveal Cell
    
    func revealHourlyCell() {
        
        if let revealIndex = revealHourlyRow {
            if let _ = hourlyTenPeriods {
                selectPeriod()
            } else {
                weatherDataStore.getHourlyTen()
            }
            let indexPath = IndexPath(row: revealIndex + 1, section: 0)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [indexPath], with: .bottom)
            self.tableView.endUpdates()
            
            self.tableView.rectForRow(at: indexPath)
            self.tableView.scrollRectToVisible(self.tableView.rectForRow(at: indexPath), animated: true)
        }
    }
    
    func dismissHourlyCell(_ animated:Bool) {
        
        if let revealIndex = revealHourlyRow {
            let indexPath = IndexPath(row: revealIndex + 1, section: 0)
            revealHourlyRow = nil
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [indexPath], with: animated ? .top : .none)
            self.tableView.endUpdates()
        }
    }
    
    // Work In Progress : perform delete and insert simultaneously
    func dismissRevealHourlyCell(_ fromIndex:Int, toIndex:Int) {
        let deleteIndexPath = IndexPath(row: fromIndex, section: 0)
        let insertIndexPath = IndexPath(row: toIndex, section: 0)
        self.tableView.beginUpdates()
        revealHourlyRow = nil
        self.tableView.deleteRows(at: [deleteIndexPath], with: .automatic)
        revealHourlyRow = toIndex - 1
        selectPeriod()
        self.tableView.insertRows(at: [insertIndexPath], with: .automatic)
        self.tableView.endUpdates()
    }
    
    // MARK:- UITableview update
    
    func updateTableForIconImage(_ iconName:String) {
        if let visible = self.tableView.indexPathsForVisibleRows {
            for indexPath in visible {
                
                var normalizedRow = indexPath.row
                var standardCell = true
                
                if let revealIndex = revealHourlyRow {
                    if normalizedRow > revealIndex + 1 {
                        normalizedRow -= 1
                    } else if normalizedRow == revealIndex + 1  {
                        standardCell = false
                    }
                }
                
                if standardCell {
                    
                    if let count = self.forecastedPeriods?.periods.count {
                        if normalizedRow < count {
                            if let forecastPeriod = forecastedPeriods?.periods[normalizedRow] {
                                if let iconURL = forecastPeriod.iconUrl, iconURL == iconName {
                                    DispatchQueue.main.async {
                                        self.tableView.beginUpdates()
                                        self.tableView.reloadRows(at: [indexPath], with: .automatic)
                                        self.tableView.endUpdates()
                                    }
                                }
                            }
                        }
                    }
                    
                    
                } else {
                    //print("non standard cell")
                }
            }
        }  // let visible
    }
    
    
    // MARK: Table view delegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let revealIndex = revealHourlyRow {
            if indexPath.row == revealIndex {
                let selectIndexPath = IndexPath(row: revealIndex, section: 0)
                self.tableView.selectRow(at: selectIndexPath, animated: false, scrollPosition: .none)
            }
        }
    }

    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let result = UITableViewHeaderFooterView()
        result.contentView.backgroundColor = UIColor.clear
        return result
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let revealIndex = revealHourlyRow {
            
            if indexPath.row == revealIndex {
                tableView.deselectRow(at: indexPath, animated: true)
                dismissHourlyCell(true)
                
            } else {
                
                var normalizedRow = indexPath.row
                if normalizedRow < revealIndex  {
                    normalizedRow += 1
                }
                
                let fromIndex = revealIndex + 1
                let toIndex = normalizedRow
                dismissRevealHourlyCell(fromIndex, toIndex: toIndex)
            }
            
        } else {
            revealHourlyRow = indexPath.row
            revealHourlyCell()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var result = 64.0
        if let revealIndex = revealHourlyRow {
            if indexPath.row == revealIndex + 1 {
                result = 100.0
            }
        }
        
        return CGFloat(result)
    }
    
    
    // MARK: Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //var resultCount = forecastPeriods.count
        var resultCount = 0
        
        if let count = self.forecastedPeriods?.periods.count {
            resultCount = count
        }
        
        if let _ = revealHourlyRow {
            resultCount += 1
        }
        
        return resultCount
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var normalizedRow = indexPath.row
        var reuseIdentifier = "WAForecastPeriodCell"
        var isStandardCell = true
        
        if let revealIndex = revealHourlyRow {
            if normalizedRow > revealIndex + 1 {
                normalizedRow -= 1
            } else if normalizedRow == revealIndex + 1  {
                isStandardCell = false
                reuseIdentifier = "ForecastRevealCell"
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        
        if isStandardCell {
            // Standard Cell

            let standardCell = cell
            var primaryText = ""
            var detailText = ""
            var iconUrlString = ""
            
            if let count = forecastedPeriods?.periods.count, normalizedRow < count {
                
                if let forecastPeriod = forecastedPeriods?.periods[normalizedRow] {
                    primaryText = forecastPeriod.title ?? ""
                    detailText = forecastPeriod.text ?? ""
                    iconUrlString = forecastPeriod.iconUrl ?? ""
                }
                
                if normalizedRow % 2 == 0 {
                    // isDataRow day not night
                    let dayIndex = normalizedRow / 2
                    if let forecastDay = forecastDays?.periods[dayIndex] {
                        primaryText += "  \(forecastDay.tempHighString)"
                        primaryText += " / \(forecastDay.tempLowString)"
                    }
                }
                
                standardCell.imageView!.image = self.weatherDataStore.imageFor(iconUrlString)
                
            } else {
                standardCell.imageView!.image = nil
            }
            
            standardCell.textLabel!.text = primaryText
            standardCell.detailTextLabel!.text = detailText
            
            
        } else {
            // Reveal Cell
            
            if let revealCell = cell as? ForecastRevealCell {

                revealCell.collectionView.dataSource = hourlyCollectionData
                //revealCell.collectionView.delegate = hourlyCollectionData
                
                var isNight = false
                if let revealIndex = revealHourlyRow {
                    isNight = isNightForRow(revealIndex)
                }

                if let _ = hourlyTenPeriods {
                    revealCell.collectionView.reloadData()
                    positionCollection(revealCell, isNight:isNight)
                } else {
                    revealCell.activity.startAnimating()
                }
            }
        }
        
        
        return cell
    }
 
    
    // MARK:- UICollectionView
    
    func positionCollection(_ revealCell:ForecastRevealCell, isNight:Bool) {
        
        if isNight {
            
            if let count = hourlyCollectionData.hourPeriods?.periods.count {
                if count > 0 {
                    let endIndex = count - 1
                    let endIndexPath = IndexPath(item: endIndex, section: 0)
                    revealCell.collectionView.scrollToItem(at: endIndexPath, at: .right, animated:false)
                }
            }
        } else {
            if let count = hourlyCollectionData.hourPeriods?.periods.count {
                if count < 23 {
                    // Less than a full day must be current day
                    let startIndex = 0
                    let startIndexPath = IndexPath(item: startIndex, section: 0)
                    revealCell.collectionView.scrollToItem(at: startIndexPath, at: .left, animated:false)
                } else {
                    // Full day
                    let startIndex = count / 2
                    let startIndexPath = IndexPath(item: startIndex, section: 0)
                    revealCell.collectionView.scrollToItem(at: startIndexPath, at: .centeredHorizontally, animated:false)
                }
            }
        }
    }
    
    func updateCollectionForIconImage(_ iconName:String) {
        
        if let revealIndex = revealHourlyRow {
            let indexPath = IndexPath(row: revealIndex + 1, section: 0)
            if let cell = self.tableView.cellForRow(at: indexPath) as? ForecastRevealCell {
                
                if let visible = cell.collectionView?.indexPathsForVisibleItems,
                    let count = hourlyCollectionData.hourPeriods?.periods.count {
                    for indexPath in visible {
                        if indexPath.item < count {
                            let hourItem = hourlyCollectionData.hourPeriods?.periods[indexPath.item]
                            if let iconURL = hourItem?.iconUrl , iconURL == iconName {
                                DispatchQueue.main.async {
                                    cell.collectionView?.reloadItems(at: [indexPath])
                                }
                            }
                        }
                        
                    }
                }  // let visible
            }
        }
    }
    
    
}


// MARK: WADataStoreDelegate

extension ForecastTableViewController: DataStoreDelegate {
    
    func dataStore(_ controller: DataStore, updateForIconImage iconName:String) {
        updateTableForIconImage(iconName)
        updateCollectionForIconImage(iconName)
    }
    
    // JSON parsed
    func dataStore(_ controller: DataStore, didReceiveForecast forecastDayData:ForecastDays, forecastPeriodsData:ForecastPeriods )
    {
        forecastedPeriods = forecastPeriodsData // 20 items day and night
        forecastDays = forecastDayData // 10 items conditions high low
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        refreshInProgress = false
        self.refreshControl?.endRefreshing()
    }
    
    // JSON parsed
    func dataStore(_ controller: DataStore, didReceiveHourlyTen hourPeriods:[HourPeriods]) {
        hourlyTenPeriods = hourPeriods
        selectPeriod()
        
        if let revealIndex = revealHourlyRow {
            let indexPath = IndexPath(row: revealIndex + 1, section: 0)
            
            if let cell = self.tableView.cellForRow(at: indexPath) as? ForecastRevealCell {
                
                DispatchQueue.main.async {
                    cell.activity.stopAnimating()
                    cell.collectionView?.reloadData()
                }
                
                DispatchQueue.main.async {
                    self.tableView.beginUpdates()
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                    self.tableView.endUpdates()
                }
            }
        }
        
        refreshHourlyInProgress = false
        
    }
    
}


// MARK: WAHourlyCollectionDataDelegate

extension ForecastTableViewController: HourlyCollectionDataDelegate {
    
    func hourlyCollection(_ controller:HourlyCollectionData, imageForIcon iconName:String) -> UIImage? {
        return self.weatherDataStore.imageFor(iconName)
    }
    
}



//                if isDataRow {
//                    let dayIndex = normalizedRow / 2
//                    if let forecastDay = forecastDays?.periods[dayIndex] {
//                        primaryText += "  \(forecastDay.tempHighString)"
//                        primaryText += " / \(forecastDay.tempLowString)"
//                    }
//                }

//                    if let titleText = forecastPeriod.title {
//                        primaryText = titleText
//                    }
//                    if let titleText = forecastPeriod.text {
//                        detailText = titleText
//                    }
//                    if let iconURL = forecastPeriod.iconUrl {
//                        iconUrlString = iconURL
//                    }




//            if indexPath.row == revealIndex + 1 {
//                reuseIdentifier = "WAForecastRevealCell"
//            }

//        if reuseIdentifier == "WAForecastRevealCell" {
//            if let revealCell = cell as? WAForecastRevealCell {
//                revealCell.collectionView.dataSource = hourlyCollectionData
//                revealCell.collectionView.delegate = hourlyCollectionData
//            }
//        }

//        if let revealIndex = revealHourlyRow {
//            if normalizedRow > revealIndex + 1 {
//                normalizedRow -= 1
//            } else if normalizedRow == revealIndex + 1  {
//                standardCell = false
//            }
//        }



//        var normalizedRow = indexPath.row
//
//        var standardCell = true
//
//        if let revealIndex = revealHourlyRow {
//            if normalizedRow > revealIndex + 1 {
//                normalizedRow -= 1
//            } else if normalizedRow == revealIndex + 1  {
//                standardCell = false
//            }
//        }
//
//        if standardCell {
//            var primaryText = ""
//            var detailText = ""
//            var iconUrlString = ""
//
//            if let count = forecastedPeriods?.periods.count {
//                if normalizedRow < count {
//
//                    if let forecastPeriod = forecastedPeriods?.periods[normalizedRow] {
//
//                        if let titleText = forecastPeriod.title {
//                            primaryText = titleText
//                        }
//
//                        if let titleText = forecastPeriod.text {
//                            detailText = titleText
//                        }
//
//                        if let iconURL = forecastPeriod.iconUrl {
//                            iconUrlString = iconURL
//                        }
//                    }
//
//                    let dayIndex = normalizedRow / 2
//
//                    var isDataRow = false
//                    if normalizedRow % 2 == 0 {
//                        isDataRow = true
//                    }
//
//                    if isDataRow {
//
//                        if let forecastDay = forecastDays?.periods[dayIndex] {
//                            primaryText += "  \(forecastDay.tempHighString)"
//                            primaryText += " / \(forecastDay.tempLowString)"
//                        }
//                    }
//
//                }
//            }
//
//            cell.textLabel!.text = primaryText
//            cell.detailTextLabel!.text = detailText
//            cell.imageView!.image = self.weatherDataStore.imageFor(iconUrlString)
//
//            if let revealIndex = revealHourlyRow {
//                if indexPath.row == revealIndex {
//                    let selectIndexPath = IndexPath(row: revealIndex, section: 0)
//                    self.tableView.selectRow(at: selectIndexPath, animated: false, scrollPosition: .none)
//                }
//            }
//
//
//        } else {
//            // Reveal Cell
//            var isNight = false
//            if let revealIndex = revealHourlyRow {
//                isNight = isNightForRow(revealIndex)
//            }
//
//            if let revealCell = cell as? WAForecastRevealCell {
//                if let _ = hourlyTenPeriods {
//                    revealCell.collectionView.reloadData()
//                    positionCollection(revealCell, isNight:isNight)
//                } else {
//                    revealCell.activity.startAnimating()
//                }
//            }
//        }
//
//    }
