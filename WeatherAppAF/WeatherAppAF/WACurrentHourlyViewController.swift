//
//  WACurrentHourlyViewController.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/24/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

// ***UNUSED IN WeatherApp
// Either sample or prototype

import UIKit

class WACurrentHourlyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

//    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "WAEmbedHourly" {
            if let _ = segue.destinationViewController as? WAHourlyCollectionViewController {
            }
        }
    }

}
