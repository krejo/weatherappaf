//
//  WAHourlyCollectionDataSource.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/25/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation
import UIKit

/// Standard cell definition
class WAHourlyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
}

/// Detail cell definition
class WAHourlyDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var skyVisibilityView: UIView!
}


struct CellReuseIdentifiers {
    static let Hourly = "WAHourlyCollectionViewCell"
    static let HourlyDetail = "WAHourlyDetailCollectionViewCell"
}

protocol HourlyCollectionDataDelegate: class {
    func hourlyCollection(_ controller: HourlyCollectionData, imageForIcon iconName:String) -> UIImage?
}


class HourlyCollectionData: NSObject {
    
    /// Delegate to obtain image
    weak var delegate: HourlyCollectionDataDelegate?
    
    /// The hour periods to process
    var hourPeriods:HourPeriods?
    
    /// whether to include day of week
    var includeDOW = false
    
    /// whether to present detail
    var useDetail = false
    
    /// Processes sky visibility within a range
    struct SkyVisability {
        let lowVisValue = 0.10
        let highVisValue = 0.90
        var skyValue = 0.0
        func skyVisValue() -> Double {
            var result = lowVisValue
            let range = highVisValue - lowVisValue
            result = lowVisValue + (range * skyValue/100.0)
            return result
        }
    }
    
    
    func configureHourlyDetailCell(_ cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let hourCell = cell as? WAHourlyDetailCollectionViewCell {
            if let hourPeriod = hourPeriods?.periods[indexPath.row] {

                var topText = ""
                var bottomText = ""
                
                if let intHour = hourPeriod.hour {
                    bottomText = "\(intHour)"
                }
                
                if let ampm = hourPeriod.ampm {
                    bottomText += " \(ampm)"
                }
                
                if includeDOW {
                    if let dow = hourPeriod.weekday {
                        bottomText += "\n\(dow)"
                    }
                }
                
                topText = hourPeriod.temperatureString
                
                hourCell.topLabel.text = topText
                hourCell.bottomLabel.text = bottomText
                
                if let iconURL = hourPeriod.iconUrl {
                    hourCell.imageView.image = delegate?.hourlyCollection(self, imageForIcon: iconURL)
                } else {
                    hourCell.imageView.image = nil
                }
                
                var precipString = ""
                if let pop = hourPeriod.possiblePrecipitation, pop > 28 {
                    let possiblePrecip = "\(pop)%"
                    precipString += possiblePrecip
                }
                
                hourCell.skyVisibilityView.alpha = 0.0
                hourCell.detailLabel.text = precipString
                if let sky = hourPeriod.skyValue {
                    let visibleSky = SkyVisability(skyValue: Double(sky)).skyVisValue()
                    hourCell.skyVisibilityView.alpha = CGFloat(visibleSky)
                }
                
            } else {
                hourCell.topLabel.text = ""
                hourCell.bottomLabel.text = ""
                hourCell.imageView.image = nil
                hourCell.skyVisibilityView.alpha = CGFloat(0.0)
                hourCell.detailLabel.text = ""
            }
        }
    }
    
    func configureHourlyStandardCell(_ cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if let hourCell = cell as? WAHourlyCollectionViewCell {
            if let hourPeriod = hourPeriods?.periods[indexPath.row] {
                
                var topText = ""
                var bottomText = ""
                
                if let intHour = hourPeriod.hour {
                    bottomText = "\(intHour)"
                }
                
                if let ampm = hourPeriod.ampm {
                    bottomText += " \(ampm)"
                }
                
                if includeDOW {
                    if let dow = hourPeriod.weekday {
                        bottomText += "\n\(dow)"
                    }
                }
                
                topText = hourPeriod.temperatureString
                
                hourCell.topLabel.text = topText
                hourCell.bottomLabel.text = bottomText
                
                if let iconURL = hourPeriod.iconUrl {
                    hourCell.imageView.image = delegate?.hourlyCollection(self, imageForIcon: iconURL)
                } else {
                    hourCell.imageView.image = nil
                }
                
            } else {
                hourCell.topLabel.text = ""
                hourCell.bottomLabel.text = ""
                hourCell.imageView.image = nil
            }
        }
    }
    
    
}



// MARK: UICollectionViewDataSource

extension HourlyCollectionData: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = hourPeriods?.periods.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if useDetail {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellReuseIdentifiers.HourlyDetail, for: indexPath)
            configureHourlyDetailCell(cell, forItemAt: indexPath)
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellReuseIdentifiers.Hourly, for: indexPath)

            configureHourlyStandardCell(cell, forItemAt: indexPath)
            return cell

        }
    }

}



// HOURLY Item

//["feelslike": {
//    english = 56;
//    metric = 13;
//    }, "icon": chancerain, "wx": Showers, "temp": {
//        english = 56;
//        metric = 13;
//    }, "pop": 60, "humidity": 80, "windchill": {
//        english = "-9999";
//        metric = "-9999";
//    }, "dewpoint": {
//        english = 50;
//        metric = 10;
//    }, "wdir": {
//        degrees = 254;
//        dir = WSW;
//    }, "uvi": 0, "snow": {
//        english = "0.0";
//        metric = 0;
//    }, "mslp": {
//        english = "30.03";
//        metric = 1017;
//    }, "condition": Chance of Rain, "heatindex": {
//        english = "-9999";
//        metric = "-9999";
//    }, "FCTTIME": {
//        UTCDATE = "";
//        age = "";
//        ampm = PM;
//        civil = "4:00 PM";
//        epoch = 1478638800;
//        hour = 16;
//        "hour_padded" = 16;
//        isdst = 0;
//        mday = 8;
//        "mday_padded" = 08;
//        min = 00;
//        "min_unpadded" = 0;
//        mon = 11;
//        "mon_abbrev" = Nov;
//        "mon_padded" = 11;
//        "month_name" = November;
//        "month_name_abbrev" = Nov;
//        pretty = "4:00 PM EST on November 08, 2016";
//        sec = 0;
//        tz = "";
//        "weekday_name" = Tuesday;
//        "weekday_name_abbrev" = Tue;
//        "weekday_name_night" = "Tuesday Night";
//        "weekday_name_night_unlang" = "Tuesday Night";
//        "weekday_name_unlang" = Tuesday;
//        yday = 312;
//        year = 2016;
//    }, "wspd": {
//        english = 9;
//        metric = 14;
//    }, "qpf": {
//        english = "0.01";
//        metric = 0;
//    }, "sky": 100, "icon_url": http://icons.wxug.com/i/c/k/chancerain.gif, "fctcode": 12]
//
//
//    ["feelslike": {
//    english = 57;
//    metric = 14;
//    }, "icon": chancerain, "wx": Few Showers, "temp": {
//    english = 57;
//    metric = 14;
//    }, "pop": 32, "humidity": 75, "windchill": {
//    english = "-9999";
//    metric = "-9999";
//    }, "dewpoint": {
//    english = 49;
//    metric = 9;
//    }, "wdir": {
//    degrees = 235;
//    dir = SW;
//    }, "uvi": 1, "snow": {
//    english = "0.0";
//    metric = 0;
//    }, "mslp": {
//    english = "30.06";
//    metric = 1018;
//    }, "condition": Chance of Rain, "heatindex": {
//    english = "-9999";
//    metric = "-9999";
//    }, "FCTTIME": {
//    UTCDATE = "";
//    age = "";
//    ampm = PM;
//    civil = "2:00 PM";
//    epoch = 1478631600;
//    hour = 14;
//    "hour_padded" = 14;
//    isdst = 0;
//    mday = 8;
//    "mday_padded" = 08;
//    min = 00;
//    "min_unpadded" = 0;
//    mon = 11;
//    "mon_abbrev" = Nov;
//    "mon_padded" = 11;
//    "month_name" = November;
//    "month_name_abbrev" = Nov;
//    pretty = "2:00 PM EST on November 08, 2016";
//    sec = 0;
//    tz = "";
//    "weekday_name" = Tuesday;
//    "weekday_name_abbrev" = Tue;
//    "weekday_name_night" = "Tuesday Night";
//    "weekday_name_night_unlang" = "Tuesday Night";
//    "weekday_name_unlang" = Tuesday;
//    yday = 312;
//    year = 2016;
//    }, "wspd": {
//    english = 11;
//    metric = 18;
//    }, "qpf": {
//    english = "0.0";
//    metric = 0;
//    }, "sky": 98, "icon_url": http://icons.wxug.com/i/c/k/chancerain.gif, "fctcode": 12]
//


