//
//  WACacheFiles.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/23/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import Foundation

/**
 An extension to NSURL to formulate fileURLS based on service call path
 */

extension URL {
    
    static func cacheFileURLFromURL(_ sourceURL: URL, delimiter:String) -> URL? {
        var resultFileURL: URL?
        var relativePathComponents = [String]()
        
        var indexToDocuments = 0
        let pathComponents = sourceURL.pathComponents
        
        for path in pathComponents {
            if path == delimiter {
                break;
            }
            indexToDocuments += 1
        }
        
        let indexPastDocuments = indexToDocuments + 1
        let lastIndex = pathComponents.count
        
        for index in indexPastDocuments..<lastIndex {
            relativePathComponents += [pathComponents[index]]
        }
        
        resultFileURL = cacheFileURL(relativePathComponents)
        
        return resultFileURL
    }
    
    fileprivate static func cacheFileURL(_ inPath:[String]?) -> URL? {
        
        var resultFileURL: URL?
        var urlPath: URL
        
        if let pathComponents = inPath  {
            if pathComponents.count > 0 {
                // Create the path with first component append remaining components
                urlPath = URL(string: pathComponents[0])!
                for index in 1..<pathComponents.count {
                    urlPath = urlPath.appendingPathComponent(pathComponents[index])
                }
                resultFileURL = cacheFileURLWithRelativePathName(urlPath.path)
            }
        }
        
        return resultFileURL
    }
    
    fileprivate static func cacheFileURLWithRelativePathName(_ pathName: String) -> URL? {
        
        if let cacheDirectory = FileManager.default.urls(for: .cachesDirectory,in: .userDomainMask).first {
            return URL(string:pathName, relativeTo:cacheDirectory)
        }
        return nil
    }
    
}


// MARK: Class CacheFiles

class CacheFiles {
    
    // MARK: public api
    
    func readCacheFile(_ fileURL : URL) -> Data? {
        var isDir: ObjCBool = false
        var result : Data?
        // If it exists and and is valid (not stale) read and use
        if FileManager.default.fileExists(atPath: fileURL.path, isDirectory: &isDir) && validFile(fileURL) {
            result = try? Data(contentsOf: fileURL)
        }
        return result
    }
    
    func writeCacheFile(_ fileURL : URL, data: Data) {
        prepareFileWrite(fileURL)
        try? data.write(to: fileURL, options: [.atomic])
    }
    
    
    // MARK: Private
    
    fileprivate func prepareFileWrite(_ fileURL : URL) {
        
        var isDir: ObjCBool = true
        let pathURL = fileURL.deletingLastPathComponent()
        if !FileManager.default.fileExists(atPath: pathURL.path, isDirectory: &isDir) {
            do {
                try FileManager.default.createDirectory(
                    at: pathURL, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("Error: \(error)")
            }
        }
    }
    
    fileprivate func validFile(_ fileURL: URL) -> Bool {
        var result = false
        var createDate : Date? = nil
        
        do {
            let attr : NSDictionary = try FileManager.default.attributesOfItem(atPath: fileURL.path ) as NSDictionary
            createDate = attr.fileCreationDate()!
        } catch {
            print("Error: \(error)")
        }
        
        if let createDate = createDate {
            let timeSince = createDate.timeIntervalSinceNow
            if (-timeSince > 60) {
                do {
                    print("CacheFile Timedout")
                    try FileManager.default.removeItem(at: fileURL)
                } catch {
                    print("Error: \(error)")
                }
            } else {
                // Still valid has not timed out
                result = true
            }
        }
        
        return result
    }
    
}
