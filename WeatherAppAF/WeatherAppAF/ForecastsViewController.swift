//
//  WAForecastsViewController.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/22/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class ForecastsViewController: UIViewController {
    
    /// UI Elements
    @IBOutlet weak var forecastSomeContainerView: UIView!
    @IBOutlet weak var forecastTenContainerView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    let animated = true
    
    enum AnimationType {
        case push
        case flip
        case flip2
        case none
    }

    let animationType = AnimationType.push
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex = 0
        
        if !animated {
            segmentValueChanged(segmentedControl)
        } else {
            forecastTenContainerView.isHidden = true
            forecastSomeContainerView.isHidden = false
        }
        
        switch animationType {
        case .flip:
            self.view.backgroundColor = UIColor.black
        case .flip2:
            self.view.backgroundColor = UIColor.black
        default:
            break
        }
    }
    
    
    /// Performs actions based on value changed.
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if animated {
                
                switch animationType {
                case .push :
                    let transition = CATransition()
                    transition.type = kCATransitionPush
                    transition.subtype = kCATransitionFromLeft
                    transition.duration = 0.45
                    transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                    transition.isRemovedOnCompletion = true
                    
                    let transition2 = CATransition()
                    transition2.type = kCATransitionPush
                    transition2.subtype = kCATransitionFromLeft
                    transition2.duration = 0.45
                    transition2.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                    transition2.isRemovedOnCompletion = true
                    
                    forecastTenContainerView.layer.add(transition, forKey: nil)
                    forecastSomeContainerView.layer.add(transition2, forKey: nil)
                    
                    forecastTenContainerView.isHidden = true
                    forecastSomeContainerView.isHidden = false
                    
                case .flip :
                    UIView.transition(with: forecastSomeContainerView, duration: 0.75, options: .transitionFlipFromLeft,
                                      animations: {
                                        self.forecastSomeContainerView.isHidden = false
                    }, completion: nil)
                    
                    UIView.transition(with: forecastTenContainerView, duration: 0.75, options: .transitionFlipFromLeft,
                                      animations: {
                                        self.forecastTenContainerView.isHidden = true
                    }, completion: nil)

                    
                case .flip2 :
                    UIView.transition(from: forecastTenContainerView, to: forecastSomeContainerView, duration: 0.5,
                                      options: [.showHideTransitionViews,.transitionFlipFromLeft,.curveEaseOut], completion: nil)

                default:
                    forecastTenContainerView.isHidden = true
                    forecastSomeContainerView.isHidden = false
                }
                
            } else {
                forecastTenContainerView.isHidden = true
                forecastSomeContainerView.isHidden = false
            }
        } else if sender.selectedSegmentIndex == 1 {
            
            if animated {
                
                switch animationType {
                case .push :
                    let transition = CATransition()
                    transition.type = kCATransitionPush
                    transition.subtype = kCATransitionFromRight
                    transition.duration = 0.45
                    transition.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                    transition.isRemovedOnCompletion = true
                    
                    let transition2 = CATransition()
                    transition2.type = kCATransitionPush
                    transition2.subtype = kCATransitionFromRight
                    transition2.duration = 0.45
                    transition2.timingFunction = CAMediaTimingFunction(name:kCAMediaTimingFunctionEaseInEaseOut)
                    transition2.isRemovedOnCompletion = true
                    
                    forecastTenContainerView.layer.add(transition, forKey: nil)
                    forecastSomeContainerView.layer.add(transition2, forKey: nil)
                    
                    forecastTenContainerView.isHidden = false
                    forecastSomeContainerView.isHidden = true
                    
                case .flip :
                    
                    UIView.transition(with: forecastSomeContainerView, duration: 0.75, options: .transitionFlipFromRight,
                                      animations: {
                                        self.forecastSomeContainerView.isHidden = true
                    }, completion: nil)
                    
                    UIView.transition(with: forecastTenContainerView, duration: 0.75, options: .transitionFlipFromRight,
                                      animations: {
                                        self.forecastTenContainerView.isHidden = false
                    }, completion: nil)

                case .flip2 :
                    UIView.transition(from: forecastSomeContainerView, to: forecastTenContainerView, duration: 0.5,
                                      options: [.showHideTransitionViews,.transitionFlipFromRight,.curveEaseOut], completion: nil)

                    
                default:
                    forecastTenContainerView.isHidden = false
                    forecastSomeContainerView.isHidden = true
                }
                
                
            } else {
                forecastTenContainerView.isHidden = false
                forecastSomeContainerView.isHidden = true
            }
        }
        
    }

}
