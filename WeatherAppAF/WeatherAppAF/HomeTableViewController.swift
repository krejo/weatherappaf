//
//  WAHomeTableViewController.swift
//  WeatherApp
//
//  Created by JOSEPH KERR on 8/28/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit
import os.log


class WAForecastDayCell: UITableViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var highTempLabel: UILabel!
    @IBOutlet weak var lowTempLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
}

class WAHourlyCell: UITableViewCell {
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
}

class WAForecastDescriptionCell: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
}

class WAHomeSecondaryCell: UITableViewCell {
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
}


/**
 Protocol to communicate the location back to the delegate
 */

protocol HomeTableDelegate : class {
    func homeTable(_ controller:HomeTableViewController, primaryLocationTitle title:String)
}


/**
 Primary home view tableViewController to display current conditions as well as
 forecast and hourly data
 */

class HomeTableViewController: UITableViewController {
    
    struct CellReuseIdentifiers {
        static let HomePrimary = "WAHomePrimaryCell"
        static let Hourly = "WAHourlyCell"
        static let ForecastDay = "WAForecastDayCell"
        static let ForecastDescription = "WAForecastDescriptionCell"
        static let HomeSecondary = "WAHomeSecondaryCell"
        static let HomeData = "WAHomeDataCell"
    }
    
    @available(iOS 10.0, *)
    static let ui_log = OSLog(subsystem: "com.joker.weatherApp", category: "UI")
    
    /// UI Elements
    @IBOutlet weak var tableHeaderLowLabel: UILabel!
    @IBOutlet weak var tableHeaderHighLabel: UILabel!
    @IBOutlet weak var tableHeaderPrimaryLabel: UILabel!
    @IBOutlet weak var tableHeaderImageView: UIImageView!
    @IBOutlet weak var tableHeaderTodayLabel: UILabel!
    @IBOutlet weak var tableHeaderTodayKeyLabel: UILabel!
    
    weak var delegate:HomeTableDelegate?
    
    /// Main title forthe view
    var primaryTitle = ""
    
    /// Obtain weather data
    var weatherDataStore = DataStore()
    
    /// All day data for many days
    var forecastDays:ForecastDays?
    
    /// Collectionview datasource for hourly
    var hourlyCollectionData = HourlyCollectionData()
    
    /// Used to determine whether refresh in progress
    var refreshInProgress = false
    
    /// Used to determine whether refresh in progress
    var refreshForecastInProgress = false
    
    /// Used to determine whether refresh in progress
    var refreshHourlyInProgress = false
    
    /// Used to determine whether refresh conditions in progress
    var refreshConditionsInProgress = false
    
    /// Stores ccurrent conditions data
    var currentConditions:CurrentConditionsData?
    
    /// The conditions of interest
    var conditionItems:[String] = []
    
    
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 10.0, *) {
            os_log("%s", log: HomeTableViewController.ui_log,  type:.info,#function)
        }
        
        weatherDataStore.delegate = self
        hourlyCollectionData.delegate = self
        hourlyCollectionData.includeDOW = true
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action:#selector(refreshTable(_:)), for:[.valueChanged])
        tableHeaderHighLabel.text = ""
        tableHeaderLowLabel.text = ""
        tableHeaderPrimaryLabel.text = ""
        tableHeaderTodayLabel.text = ""
        tableHeaderTodayKeyLabel.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("\(String(describing: self)) \(#function)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("\(String(describing: self)) \(#function)")
        
        if #available(iOS 10.0, *) {
            os_log("%s", log: HomeTableViewController.ui_log,  type:.info,#function)
        }
        
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.refreshTable(nil)
        }
    }
    
    // MARK: Refresh
    
    func refreshTable(_ control:AnyObject?) {
        
        if #available(iOS 10.0, *) {
            os_log("%s", log: HomeTableViewController.ui_log, type:.info, #function)
        }
        
        if !refreshInProgress {
            if control == nil {
                // Programmatically started
                self.refreshControl?.beginRefreshing()
            }
            
            refreshConditionsInProgress = true
            refreshForecastInProgress = true
            refreshHourlyInProgress = true
            refreshInProgress = true
            
            weatherDataStore.getCurrentConditions() { (result:ServiceRequestError) in
                // Errorhandler
                //print(result)
                self.refreshConditionsInProgress = false
                self.checkRefreshFinished()
            }
            
            weatherDataStore.getForecastTen() { (result:ServiceRequestError) in
                // Errorhandler
                //print(result)
                self.refreshForecastInProgress = false
                self.checkRefreshFinished()
            }
            
            weatherDataStore.getHourly() { (result:ServiceRequestError) in
                // Errorhandler
                //print(result)
                self.refreshHourlyInProgress = false
                self.checkRefreshFinished()
            }
        }
    }
    
    func checkRefreshFinished() {
        if !refreshConditionsInProgress && !refreshForecastInProgress && !refreshHourlyInProgress {
            // ALL of the refreshes are complete
            //print("\(#function) finished!")
            self.refreshInProgress = false
            
            DispatchQueue.main.async {
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
            }
            self.updateTableHeaderData()
        }
    }
    
    
    func updateTableHeaderData() {
        if (forecastDays?.periods.count)! > 0 {
            // First day data = Today
            let todayForecastDayData = forecastDays?.periods[0]
            tableHeaderHighLabel.text = todayForecastDayData?.tempHighString
            tableHeaderLowLabel.text = todayForecastDayData?.tempLowString
            tableHeaderTodayLabel.text = todayForecastDayData?.weekday
            
            tableHeaderTodayKeyLabel.isHidden = false
        }
        tableHeaderPrimaryLabel.text = currentConditions?.temp
    }
    
    
    // MARK: - Tableview
    
    func updateTableForIconImage(_ iconName:String) {
        if let visible = self.tableView.indexPathsForVisibleRows {
            for indexPath in visible {
                // Update section 2 items
                // Ignore first one as it represents Today and is handled differently
                let daysMaxIndex = (self.forecastDays?.periods.count)! - 1
                
                if indexPath.section == 2 && indexPath.row < daysMaxIndex {
                    
                    // Add +1 to row Ignore first one.  Row 0 is the second period
                    let indexForRow = indexPath.row + 1
                    if let forecastPeriod = forecastDays?.periods[indexForRow] {
                        
                        if let iconURL = forecastPeriod.iconUrl , iconURL == iconName {
                            DispatchQueue.main.async {
                                self.tableView.beginUpdates()
                                self.tableView.reloadRows(at: [indexPath], with: .automatic)
                                self.tableView.endUpdates()
                            }
                        }
                    }
                }
            }
        }  // let visible
    }
    
    
    // MARK: - Configure Cells Methods
    
    func configureDisplayPrimaryCell(_ cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //WAHomePrimaryCell = standard cell
        
        let conditionItem = currentConditions?.primaryItems[indexPath.row]
        cell.textLabel!.text = conditionItem?.key
        cell.detailTextLabel!.text = conditionItem?.value
    }
    
    func configureDisplayHourlyCell(_ cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let hourlyCell = cell as? WAHourlyCell {
            hourlyCell.collectionView?.reloadData()
            if !refreshInProgress {
                hourlyCell.activity.stopAnimating()
            }
        }
    }
    
    func configureDisplayForecastDayCell(_ cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let dayCell = cell as? WAForecastDayCell , (forecastDays?.periods.count)! > 0 {
            
            // Add +1 to row Ignore first one.  Row 0 is the second period
            let indexForRow = indexPath.row + 1
            if let forecastDayData = forecastDays?.periods[indexForRow] {
                dayCell.highTempLabel.text = forecastDayData.tempHighString
                dayCell.lowTempLabel.text = forecastDayData.tempLowString
                dayCell.dayLabel.text = forecastDayData.weekday
                if let iconURL = forecastDayData.iconUrl {
                    dayCell.iconImageView!.image = self.weatherDataStore.imageFor(iconURL)
                }
                
            }
        }
    }
    
    func configureDisplayForecastDescriptionCell(_ cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let descriptionCell = cell as? WAForecastDescriptionCell {
            var descriptionText = ""
            if let forecasts = forecastDays, forecasts.periods.count > 0 {
                if let forecastPeriodFirstDay = forecastDays?.periods[0].textForecastDay {
                    descriptionText += forecastPeriodFirstDay
                }
                
                if let forecastPeriodFirstNight = forecastDays?.periods[0].textForecastNight {
                    descriptionText += " " + forecastPeriodFirstNight
                }
            }
            
            descriptionCell.descriptionLabel.text = descriptionText
        }
    }
    
    func configureDisplayHomeSecondaryCell(_ cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if let secondaryCell = cell as? WAHomeSecondaryCell {
            let conditionItem = currentConditions?.secondaryItems[indexPath.row]
            secondaryCell.keyLabel!.text = conditionItem?.key
            secondaryCell.valueLabel!.text = conditionItem?.value
        }
    }
    
    func configureDisplayHomeDataCell(_ cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //WAHomeDataCell = standard cell
        
        let conditionItem = currentConditions?.conditionItems[indexPath.row]
        cell.textLabel!.text = conditionItem?.key
        cell.detailTextLabel!.text = conditionItem?.value
    }
    
    
    // MARK: collectionView for hourly
    
    func updateCollectionForIconImage(_ iconName:String) {
        
        let hourlyCellIndexPath = IndexPath(row: 0, section: 1)
        
        if let cell = self.tableView.cellForRow(at: hourlyCellIndexPath) as? WAHourlyCell,
            let visible = cell.collectionView?.indexPathsForVisibleItems {
            
            var count = 0
            
            for indexPath in visible {
                if indexPath.item < (hourlyCollectionData.hourPeriods?.periods.count)! {
                    
                    if let hourPeriod = hourlyCollectionData.hourPeriods?.periods[indexPath.item],
                        let iconURL = hourPeriod.iconUrl , iconURL == iconName {
                        
                        DispatchQueue.main.async {
                            cell.collectionView?.reloadItems(at: [indexPath])
                        }
                        count += 1
                    }
                }
            }  // let visible
            
            //print("\(count) items updated")
        }
    }
    
}



// Mark: WAHourlyCollectionDataDelegate

extension HomeTableViewController: HourlyCollectionDataDelegate {
    
    func hourlyCollection(_ controller:HourlyCollectionData, imageForIcon iconName:String) -> UIImage? {
        return self.weatherDataStore.imageFor(iconName)
    }
}


// MARK: - Table view delegate

extension HomeTableViewController {
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            // "WAHomePrimaryCell"
            configureDisplayPrimaryCell(cell, forRowAt: indexPath)
            
        } else if indexPath.section == 1 {
            // "WAHourlyCell"
            configureDisplayHourlyCell(cell, forRowAt: indexPath)
            
        } else if indexPath.section == 2 {
            // "WAForecastDayCell"
            configureDisplayForecastDayCell(cell, forRowAt: indexPath)
            
        } else if indexPath.section == 3 {
            // "WAForecastDescriptionCell"
            configureDisplayForecastDescriptionCell(cell, forRowAt: indexPath)
            
        } else if indexPath.section == 4 {
            // "WAHomeSecondaryCell"
            configureDisplayHomeSecondaryCell(cell, forRowAt: indexPath)
            
        } else if indexPath.section == 5 {
            // "WAHomeDataCell"
            configureDisplayHomeDataCell(cell, forRowAt: indexPath)
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var result = 42.0 // primary
        if indexPath.section == 1 {
            result = 100.0 // hourly
        } else if indexPath.section == 2 {
            result = 38.0 // forecastdays
        } else if indexPath.section == 3 {
            result = 102.0 // forecastdescription
        } else if indexPath.section == 4 {
            result = 32.0 // secondary
        }
        
        return CGFloat(result)
    }
    
}


// MARK: - Table view data source

extension HomeTableViewController {
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return primaryTitle
        }
        
        return ""
    }
    
    /*
     section 0  - primary
     section 1  - hourly
     section 2  - forecastdays
     section 3  - forecastdescription
     section 4  - secondary
     section 5  - otheritems
     */
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            if let count = currentConditions?.primaryItems.count {
                return count
            } else {
                return 0
            }
            
            //return primaryItems.count
        } else if section == 1 {
            return 1
        } else if section == 2 {
            if let count = forecastDays?.periods.count, count > 0 {
                return count - 1
            } else {
                return 0
            }
        } else if section == 3 {
            return 1
        } else if section == 4 {
            if let count = currentConditions?.secondaryItems.count {
                return count
            } else {
                return 0
            }
            
            
        } else if section == 5 {
            if let count = currentConditions?.conditionItems.count {
                return count
            } else {
                return 0
            }
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var reuseIdentifier = CellReuseIdentifiers.HomeData
        
        if indexPath.section == 0 {
            reuseIdentifier = CellReuseIdentifiers.HomePrimary
        } else if indexPath.section == 1 {
            reuseIdentifier = CellReuseIdentifiers.Hourly
        } else if indexPath.section == 2 {
            reuseIdentifier = CellReuseIdentifiers.ForecastDay
        } else if indexPath.section == 3 {
            reuseIdentifier = CellReuseIdentifiers.ForecastDescription
        } else if indexPath.section == 4 {
            reuseIdentifier = CellReuseIdentifiers.HomeSecondary
        } else if indexPath.section == 5 {
            reuseIdentifier = CellReuseIdentifiers.HomeData
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        if reuseIdentifier == CellReuseIdentifiers.Hourly {
            if let hourlyCell = cell as? WAHourlyCell {
                hourlyCell.collectionView.dataSource = hourlyCollectionData
                //hourlyCell.collectionView.delegate = hourlyCollectionData
            }
        }
        
        return cell
    }
}


// MARK: - WADataStoreDelegate

extension HomeTableViewController: DataStoreDelegate {
    
    func dataStore(_ controller: DataStore, didReceiveCurrentConditions conditionItems:[String], conditions:CurrentConditionsData) {
        if #available(iOS 10.0, *) {
            os_log("callback %s", log: HomeTableViewController.ui_log, type:.info, #function)
        }
        
        self.currentConditions = conditions
        self.conditionItems = conditionItems
        self.currentConditions?.primaryItems = []
        primaryTitle = (self.currentConditions?.weather)!
        refreshConditionsInProgress = false
        checkRefreshFinished()
        
        if #available(iOS 10.0, *) {
            os_log("callback %s %ul", log: HomeTableViewController.ui_log, type:.debug, #function, conditionItems.count)
        }
        
    }
    
    func dataStore(_ controller: DataStore, didReceiveForecast forecastData:ForecastDays) {
        if #available(iOS 10.0, *) {
            os_log("callback %s count %ul", log: HomeTableViewController.ui_log, type:.debug, #function,
                   forecastData.periods.count )
        }
        forecastDays = forecastData
        refreshForecastInProgress = false
        checkRefreshFinished()
    }
    
    func dataStore(_ controller: DataStore, didReceiveForecast forecastDayData:ForecastDays, forecastPeriodsData:ForecastPeriods )
    {
        if #available(iOS 10.0, *) {
            os_log("callback %s count %ul", log: HomeTableViewController.ui_log, type:.debug, #function,
                   forecastDayData.periods.count )
        }
        // ignore periods data
        forecastDays = forecastDayData
        refreshForecastInProgress = false
        checkRefreshFinished()
    }
    
    func dataStore(_ controller: DataStore, didReceiveHourly hourPeriods:HourPeriods) {
        hourlyCollectionData.hourPeriods = hourPeriods
        refreshHourlyInProgress = false
        checkRefreshFinished()
    }
    
    func dataStore(_ controller: DataStore, primaryLocationTitle:String) {
        DispatchQueue.main.async {
            self.delegate?.homeTable(self, primaryLocationTitle: primaryLocationTitle)
        }
    }
    
    func dataStore(_ controller: DataStore, updateForIconImage iconName:String) {
        if !refreshInProgress {
            // Is it the header view icon
            if let iconURL = currentConditions?.iconUrl {
                if iconURL == iconName {
                    DispatchQueue.main.async {
                        self.tableHeaderImageView.image = controller.imageFor(iconName)
                    }
                }
            }
            
            // Update table and collectionView
            updateTableForIconImage(iconName)
            updateCollectionForIconImage(iconName)
        }
    }
    
}

