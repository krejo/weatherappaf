# README #


Is an iOS app that uses the WeatherUnderGround API. Demonstrates use of both NSURLSession and AlamoFire

Swift3

### Purpose ###

* Demonstrate iOS functionality

### Xcode8 ###

* Clone master
* Open workspace in Xcode8
* Dependencies - Xcode8
* AlamoFire 4
* SwiftyJSON
* Supports iOS9+

## Choose an implementation ##

* Alamofire
* URLSession

`WAWeatherInfo` depends on an implementation `wuServiceRequest`. The super class `WAWeatherServiceNS` and `WAWeatherServiceAF` both implement this method, but differently.

    func wuServiceRequest(_ service: String, errorHandler:((_ result:WAServiceRequestError) -> Void)?, processResponse:@escaping (_ response:AnyObject?) -> Void ) {


### Using an Alamofire implementation ###

To use Alamofire, make `WAWeatherInfo` a subclass of `WAWeatherServiceAF`

    class WAWeatherInfo: WAWeatherServiceAF {


### Using an URLSession implementation ###

To use an implementation of `URLSession` , make `WAWeatherInfo` a subclass of `WAWeatherServiceNS`

    class WAWeatherInfo: WAWeatherServiceNS {


## User Interface Sample ##

### Home Screen ###

* A table view with multiple kinds of cells
* Collection view in one cell for hourly

![Alt text](/../screenshots-xc8/screens/home.png?raw=true "Home Screen")

### Daily ###

* Page through each day for daily forecast
* Uses a container for each page 
* 

![Alt text](/../screenshots-xc8/screens/daily.png?raw=true "Daily")
![Alt text](/../screenshots-xc8/screens/layers_daily.png?raw=true "Daily Layers")

### Forecast ###

* Hour by hour for each day
* Includes day and night forecast
* Reveals hourly by selecting cell
*

![Alt text](/../screenshots-xc8/screens/hourly.png?raw=true "Hourly")

### Sattelite ###

* Retrieves sattlite image
* 

![Alt text](/../screenshots-xc8/screens/sattelite.png?raw=true "Sattelite")

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Other branches of interest ###

Branch xcode7 opens and build in Xcode731 Consists of Swift2.2 code